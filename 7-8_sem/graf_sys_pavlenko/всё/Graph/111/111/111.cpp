#include "stdafx.h"
#include <glut.h>
#include <glaux.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>

unsigned int photo_tex; // ������������� ��������
AUX_RGBImageRec *AS;// ������������� ��������
GLfloat BackCol[3]={1.0, 1.0, 1.0};
GLdouble dL = 8.0;// ��� �� �������
GLfloat dB = 8.0; // ��� �� ������ 
GLdouble R=3;// R - ������ ����
GLdouble px=0, py=0, pz=0; //����������� ����� x,y,z
GLfloat dlL = 0.5;
GLfloat xL = 0;
GLfloat yL = 2;
GLfloat zL = 7;
GLfloat t = 0;
GLfloat o = 0;
int n=0;
GLfloat r = 0;

void carcas ()//  ������ ��������� �����������
{
 	GLdouble x, y,z, B, L; //h - ������, L - �������
	for (L=-180; L<=180; L+=dL)//������ �� ����������
	{
		glBegin(GL_LINE_STRIP);
		for( B = -90; B <= 90; B+=dB )
		{
			x = R * cos(B * 3.14/180) * sin(L * 3.14/180)+px;
 			y = R * cos(B * 3.14/180) * cos(L * 3.14/180)+py; 
	     	z = R * sin(B * 3.14/180);	 				
			if(z>R/2) z= z+2.5*R*(z/R-0.5)*(z/R-0.5)+pz; 
			else z+=pz;
			glVertex3d(x,y,z);
		}
		glEnd();
	}
	for( B = -90; B <= 90; B+=dB )// ������ �� ����������
	{
		glBegin(GL_LINE_STRIP);
		for (L=-180; L<=180; L+=dL)
		{
			x = R * cos(B * 3.14/180) * sin(L * 3.14/180)+px;
 			y = R * cos(B * 3.14/180) * cos(L * 3.14/180)+py; 
	     	z = R * sin(B * 3.14/180);	 				
			if(z>R/2) z= z+2.5*R*(z/R-0.5)*(z/R-0.5)+pz; else z+=pz;
			glVertex3d(x,y,z);
		}
		glEnd();
	}
}





void polygon( int type )// polygon() - ������ ������ � ���� ��������������
{
	GLdouble  B, L, x[4], y[4], z[4], a[3], b[3], I[3];

	
		for( B = -90; B <= 90; B += dB )
		for( L = -180; L <= 180; L += dL )
		{
			x[0] = R * cos(B * 3.14/180) * sin(L * 3.14/180)+px;
			y[0] = R * cos(B * 3.14/180) * cos(L * 3.14/180)+py; 
			z[0] = R * sin(B * 3.14/180);
			if(z[0]>R/2) z[0]= z[0]+2.5*R*(z[0]/R-0.5)*(z[0]/R-0.5)+pz; 
			else z[0]+=pz;

			  
			x[1] = R * cos((B + dB) * 3.14/180) * sin(L * 3.14/180)+px;
 			y[1] = R * cos((B + dB) * 3.14/180) * cos(L * 3.14/180)+py; 
			z[1] = R * sin((B + dB) * 3.14/180);
			if(z[1]>R/2) z[1]= z[1]+2.5*R*(z[1]/R-0.5)*(z[1]/R-0.5)+pz;
			else z[1]+=pz;


			  
			x[2] = R * cos((B + dB) * 3.14/180) * sin((L + dL) * 3.14/180)+px;
 			y[2] = R * cos((B + dB) * 3.14/180) * cos((L + dL) * 3.14/180)+py; 
            z[2] = R * sin((B + dB) * 3.14/180);
			if(z[2]>R/2) z[2]= z[2]+2.5*R*(z[2]/R-0.5)*(z[2]/R-0.5)+pz;
			else z[2]+=pz;


		  	  			
			x[3] = R * cos(B * 3.14/180) * sin((L + dL) * 3.14/180)+px;
 			y[3] = R * cos(B * 3.14/180) * cos((L + dL) * 3.14/180)+py; 
			z[3] = R * sin(B * 3.14/180);
			if(z[3]>R/2) z[3]= z[3]+2.5*R*(z[3]/R-0.5)*(z[3]/R-0.5)+pz;
			else z[3]+=pz;


			if( type == 1 )
			{
				glBegin(GL_POLYGON);
					glVertex3d(x[0],y[0],z[0]);
					glVertex3d(x[1],y[1],z[1]);
					glVertex3d(x[2],y[2],z[2]);
					glVertex3d(x[3],y[3],z[3]);
				glEnd();
			}
			else
			{
				
 GLfloat mat_specular[]={1.0,0.0,1.0,1.0};   //���������� ������������
 GLfloat mat_shininess[]={10.0};      //����������� �������� ������
 GLfloat light_position[]={xL + 4.0,yL + 3.0, zL + 1.0, o+0.0};  //������� �����
 GLfloat white_light[]={1.0,1.0-r ,1.0-r,10.0};   //���� �����

 glMaterialfv(GL_FRONT,GL_SPECULAR,mat_specular); 
 glMaterialfv(GL_FRONT,GL_SHININESS,mat_shininess); 

 glLightfv(GL_LIGHT0,GL_POSITION,light_position);
 glLightfv(GL_LIGHT0,GL_DIFFUSE,white_light);  
 glLightfv(GL_LIGHT0,GL_SPECULAR,white_light);  

 glEnable(GL_COLOR_MATERIAL);   
 glEnable(GL_LIGHTING);       
 glEnable(GL_LIGHT0);   
				glColor3f(1.0-t,1.0-t, 1.0); 
				
				glBegin(GL_POLYGON);
					glVertex3d(x[0],y[0],z[0]);
					glVertex3d(x[1],y[1],z[1]);
					glVertex3d(x[2],y[2],z[2]);
					glVertex3d(x[3],y[3],z[3]);
					
				glEnd();
					
			}
		}
}

void carcas_del_line ()// carcas_del_line() - ������� ��������� �����
{
 	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	glColor3f(1.0,1.0,0.0);
	polygon(1);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1,1);
	glColor3f(0,0,0);
	polygon(1);
	glDisable(GL_POLYGON_OFFSET_FILL);
}
void acsonometriya()  // ����������������� ��������
{                  // ������� ������ ��� X �� 45 ��������
                   // ������ ��� Z �� 45 ��������
                   // ������������ ������������� �� ��������� Y=0

	GLdouble project[16] = { 0.525, 0.0, 0.0, 0.0,
							0.447, 0.0, -0.851, 0.0,
							0.724, 0.0, 0.525, 0.0,
							0.0, 0.0, 0.0, 1.0};
	glMatrixMode( GL_PROJECTION );
	glMultMatrixd(project);
	glMatrixMode( GL_MODELVIEW );
}

void perspectiva() //������������� �������� (������� ������ ��� Y �� 45,
                   // ����������� �� 1 ������� ����� ��� Z, 
                   //������������� �� Z=0)
{
	GLdouble perspect[16] = {0.525,   0.0,  -0.851,   0.1,
							0.0,     0.0,  0.0,   0.10,
							0.851,   0.0,  0.525,  0.10,
							0.5,	 1.0,   1.0,	   1.0};

    glMatrixMode( GL_PROJECTION );
	glMultMatrixd(perspect);
	glMatrixMode( GL_MODELVIEW );
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
 
	switch(n)
	{glColor3f(1.0, 1.0, 1.0);
		case 0: carcas(); break;
		case 1:  glColor3f(1.0, 1.0, 0.0); carcas_del_line(); break;
		case 2:  glColor3f(1.0, 1.0, 0.0); polygon(1); break;
		case 3:  glColor3f(1.0, 1.0, 0.0); polygon(2); break;
		case 4:{   
			glColor3f(1.0, 1.0, 1.0); 
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_TEXTURE_GEN_S);
			glEnable(GL_TEXTURE_GEN_T);
			glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
			glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
   			polygon(1);
	
			glDisable(GL_TEXTURE_GEN_S);
			glDisable(GL_TEXTURE_GEN_T);
			glDisable(GL_TEXTURE_2D);
		//glutSwapBuffers();
		
			break;
			   }
			
		default: n = 0; carcas(); break;
	}
	
	glFlush();
	glutSwapBuffers();
}

void init(void) 
{
//    glEnable(GL_COLOR_MATERIAL); 
	glClearColor (0.0, 0.0, 0.0, 0.0);  // ����� ����� ��������� ������ ������
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho(-10.0,10.0,-10.0,10.0,-12.0,12.0);   // ����� ������� ����������
    gluLookAt( 0,0,5, 0,1,0, 0,1,0 );      // ��������� �����������
    glMatrixMode( GL_MODELVIEW);
	//povorotKap();
}

void keyboard(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 27:  exit(-1);
		case 'q': n--;   display(); break;
		case 'w': n++;                  display(); break;
		//����������������� ��������
		case 'a': acsonometriya();		display(); break;
		
		//������������� ��������
		case 'p': perspectiva();        display(); break; 
	
		//�������� ������ ��� �
		case 'x': glRotatef(10,1,0,0);  display(); break;
		case 'X': glRotatef(-10,1,0,0); display(); break;
		//�������� ������ ��� Y
		case 'c': glRotatef(10,0,1,0);  display(); break;
		case 'C': glRotatef(-10,0,1,0); display(); break;
		//�������� ������ ��� Z
		case 'z': glRotatef(10,0,0,1);  display(); break;
		case 'Z': glRotatef(-10,0,0,1); display(); break;
		//����������� ������� ����������� ��� �
		case 'b': px +=0.3;             display(); break;
		case 'B': px -=0.3;             display(); break;
		//����������� ������� ����������� ��� Y
		case 'n': pz +=0.3;             display(); break;
		case 'N': pz -=0.3;             display(); break;
		//����������� ������� ����������� ��� Z
		case 'm': py +=0.3;             display(); break;
		case 'M': py -=0.3;             display(); break;
		//����������� ��������� ����� ����������� ��� �
		case 'y': xL +=dlL;             display(); break;
		case 'Y': xL -=dlL;             display(); break;
		//����������� ��������� ����� ����������� ��� Y
		case 'u': yL +=dlL;             display(); break;
		case 'U': yL -=dlL;             display(); break;
		//����������� ��������� ����� ����������� ��� Z
		case 'i': zL +=dlL;             display(); break;
		case 'I': zL -=dlL;             display(); break;
		case 't':  t += 0.05;      display(); break;
		case 'T':  t -= 0.05;      display(); break;
		case 'r':  r += 0.05 ; display(); break; 
	      case 'R':  r -= 0.05 ; display(); break; 
			  case 'o':  o += 0.5 ; display(); break; 
	      case 'O':  o -= 0.5 ; display(); break; 
		case 'l': {
			n=0;  
			dL = 8.0;// ��� �� �������
dB = 8.0; // ��� �� ������ 
R=3.4;// R - ������ ����
 px=0, py=0, pz=0; //����������� ����� x,y,z
 dlL = 0.5;
 xL = 0;
 yL = 2;
 zL = 7;
//r = 0.5;
 t = 0;
display();break;
				  }
		default: break;
	}
}   

int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE|GLUT_DEPTH); // ����� ����������� ����������
   glutInitWindowSize (600, 600);     // ������ ����
   glutInitWindowPosition (200, 100); // ��������� ����
   glutCreateWindow ("����� ������� 6");        // �������� ����
   init ();                           // ���������� ��������� ���������
   glutDisplayFunc(display); 
   glutKeyboardFunc(keyboard); 
   glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  AS = auxDIBImageLoad(L"droplet.bmp");
   glGenTextures(1,&photo_tex);
   glBindTexture(GL_TEXTURE_2D, photo_tex);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
   glTexImage2D(GL_TEXTURE_2D, 0, 3,
             AS->sizeX,
             AS->sizeY,
             0, GL_RGB, GL_UNSIGNED_BYTE,
             AS->data);
   glutDisplayFunc(display); 
   glutMainLoop();
   return 0;
}