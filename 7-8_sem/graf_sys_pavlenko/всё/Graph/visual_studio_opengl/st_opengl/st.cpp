#define STRICT
#include <windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <math.h>
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam); 
void InitMyOpenGL(HWND hWnd);
void ResizeMyOpenGL(HWND hWnd); 
void CloseMyOpenGL(HWND hWnd);
void DrawMyExampleOpenGL(HWND hWnd);
int WINAPI WinMain (HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR lpszCmd,int nCmdShow)
{
MSG msg; 
HWND hWnd;
WNDCLASS WndClass;

WndClass.style = NULL; 
WndClass.lpfnWndProc = WndProc;
WndClass.cbClsExtra = 0;
WndClass.cbWndExtra = 0;
WndClass.hInstance = hInstance;
WndClass.hCursor = LoadCursor (NULL, IDC_ARROW);
WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION); 
WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); 
WndClass.lpszMenuName ="IDR_MENU1"; //"STUDEX�ENU";
WndClass.lpszClassName= "StudEx";


if (!RegisterClass(&WndClass)) return 0 ;
 hWnd = CreateWindow ("StudEx",
					"������� ������",
WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN |   
WS_CLIPSIBLINGS |
WS_CAPTION |  WS_SYSMENU, 
CW_USEDEFAULT, 
CW_USEDEFAULT,
600,
450, NULL, NULL,
hInstance, NULL);
if (!hWnd) return NULL; 
ShowWindow(hWnd, nCmdShow); 
UpdateWindow(hWnd);
while (GetMessage(&msg,NULL,NULL,NULL) )
{
TranslateMessage(&msg) ; 
DispatchMessage(&msg);
}
return msg.wParam;
}
LRESULT CALLBACK WndProc(HWND hWnd,UINT message,WPARAM wParam, LPARAM lParam)
{
switch (message)
{
 case WM_CREATE:
	InitMyOpenGL(hWnd); 
	break;
 case WM_SIZE: 
	ResizeMyOpenGL(hWnd); 
	break;
case WM_COMMAND:
switch (LOWORD(wParam))
{
case 201:	//����� ���� ������� 
	DrawMyExampleOpenGL(hWnd);
	break; 
case 108:
	DestroyWindow(hWnd);
	break;
default:
	break;
}
break;
case WM_DESTROY:
CloseMyOpenGL(hWnd); 
PostQuitMessage(0); 
break;
default:return DefWindowProc(hWnd, message,wParam, lParam);
	}
return 0L;
}
void   InitMyOpenGL(HWND hWnd)
{
HDC hdc;
HGLRC hglrc; 
PIXELFORMATDESCRIPTOR pfd;
int iPixelFormat;

memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR)); 
pfd.nSize =   sizeof(PIXELFORMATDESCRIPTOR); 
pfd.nVersion = 1;
pfd.dwFlags = PFD_DRAW_TO_WINDOW |PFD_SUPPORT_OPENGL;
pfd.iPixelType = PFD_TYPE_RGBA ; 
pfd.cColorBits = 24;
pfd. cDepthBits = 32;
pfd. iLayerType = PFD_MAIN_PLANE;

//---������ ������� �������� ����������� OpenGL (hglrc)---�
//---�o ������� ��������� ���� (hdc)----------------------� 
hdc = GetDC(hWnd);
iPixelFormat = ChoosePixelFormat(hdc, &pfd); 
SetPixelFormat(hdc, iPixelFormat, &pfd); 
hglrc = wglCreateContext(hdc);
if (hglrc) wglMakeCurrent(hdc, hglrc);


	}
//-----��������� ��������� ��������� �������� o��a-----� 
void ResizeMyOpenGL(HWND hWnd)
{
RECT rc;

GetClientRect(hWnd,&rc); 
glViewport(0,0,rc.right, rc.bottom);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
}


//--�������� �������� ����������: ������������ ���������� 11-----------� ��������� ����������� OpenGL-----------� 
void CloseMyOpenGL(HWND hWnd)
{
HGLRC hglrc;
HDC hdc;

hglrc = wglGetCurrentContext();
  if(hglrc)
{
hdc = wglGetCurrentDC();
 wglMakeCurrent(NULL, NULL); 
ReleaseDC (hWnd, hdc); 
wglDeleteContext(hglrc);
}
}
void DrawMyExampleOpenGL(HWND hWnd)
{
	
	glViewport(0, 200, 200, 200);
	glBegin(GL_LINE_LOOP);
	glColor3d(0, 0, 0);
	glVertex2d(-5, -5);
	glVertex2d(-5, 5);
	glVertex2d(5, 5);
	glVertex2d(5, -5);
	glEnd();

	glBegin(GL_POINTS);
	glPointSize(5);
	glColor3d(255, 0, 255);
	glVertex2d(-3, 3);
	glVertex2d(0, 3);
	glVertex2d(-4, 0);
	glVertex2d(-2, -2);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glLineWidth(1.0);
	glColor3d(0, 0, 0);
	glVertex2d(-2, 2);
	glVertex2d(2, 2);
	glVertex2d(0, -1);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glLineWidth(1.0);
	glColor3d(0, 0, 255);
	glVertex2d(1, -1);
	glVertex2d(4, -1);
	glVertex2d(4, -4);
	glVertex2d(1, -4);
	glEnd();

	glViewport(200, 200, 200, 200);

	glBegin(GL_LINE_LOOP);
	glColor3d(0, 0, 0);
	glVertex2d(-5, -5);
	glVertex2d(-5, 5);
	glVertex2d(5, 5);
	glVertex2d(5, -5);
	glEnd();

	glBegin(GL_POINTS);
	glPointSize(5);
	glColor3d(255, 0, 0);
	glVertex2d(-4, 4);
	glVertex2d(-4, 1);
	glVertex2d(-1, 4);
	glVertex2d(-1, 1);
	glVertex2d(1, -1);
	glVertex2d(4, -1);
	glVertex2d(1, -4);
	glVertex2d(4, -4);
	glEnd();

	glBegin(GL_LINES);
	glColor3d(255, 0, 0);
	glLineWidth(1.0);
	glVertex2d(-3, 2);
	glVertex2d(-2, 3);
	glVertex2d(-4, -2);
	glVertex2d(-2, 0);
	glVertex2d(0, 2);
	glVertex2d(2, 4);
	glEnd();

	glBegin(GL_LINES);
	glColor3d(255, 0, 0);
	glLineWidth(1.0);
	glVertex2d(-4, -4);
	glVertex2d(-2, -2);
	glVertex2d(-1, -1);
	glVertex2d(1, 1);
	glVertex2d(2, 2);
	glVertex2d(4, 4);
	glEnd();

	glBegin(GL_LINES);
	glColor3d(255, 0, 0);
	glLineWidth(1.0);
	glVertex2d(-2, -4);
	glVertex2d(0, -2);
	glVertex2d(2, 0);
	glVertex2d(4, 2);
	glVertex2d(2, -3);
	glVertex2d(3, -2);
	glEnd();

	glViewport(0, 0, 200, 200);

	glBegin(GL_TRIANGLES);
	glColor3d(0, 1, 0);
	glVertex2d(-4, 1);
	glVertex2d(-2, 4);
	glVertex2d(0, 1);
	glEnd();

	glBegin(GL_QUADS);
	glColor3d(0, 0, 1);
	glVertex2d(-4, -4);
	glVertex2d(-4, -1);
	glVertex2d(-1, -1);
	glVertex2d(-1, -4);

	glEnd();

	glBegin(GL_POLYGON);
	glColor3d(1, 0, 0);
	glVertex2d(0, -1);
	glVertex2d(1, 1);
	glVertex2d(3, 1);
	glVertex2d(4, -1);
	glVertex2d(3, -3);
	glVertex2d(1, -3);
	glEnd();


	glViewport(200, 0, 200, 200);
	glBegin(GL_TRIANGLE_STRIP);
	glColor3d(0, 0, 0);
	glVertex2d(-5, 0);
	glVertex2d(-5, -3);
	glVertex2d(-4, 0);

	glColor3d(1, 1, 0);
	glVertex2d(-4, 0);
	glVertex2d(-5, -3);
	glVertex2d(-3, -3);
	glEnd();

	glFlush();
//  SwapBuffers(wglGetCurrentDC());

}