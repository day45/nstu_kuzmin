import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

public class DbTableModel extends DefaultTableModel {

    private ArrayList<String> columnNames;

    private ArrayList<ArrayList<String>> matrix;

    public DbTableModel() {
    }

    @Override
    public int getRowCount() {
        return matrix==null ? 0 : matrix.size();
    }

    @Override
    public int getColumnCount() {
       if(columnNames == null) return 1;
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(matrix == null || matrix.get(rowIndex) == null)
            return null;
        return matrix.get(rowIndex).get(columnIndex);
    }

    @Override
    public String getColumnName(int columnIndex) {
        if(columnNames == null) return null;
        return columnNames.get(columnIndex);
    }

    public void setMatrix(ArrayList<ArrayList<String>> matrix) {
        this.matrix = matrix;
    }

    public void update() {
        setColumnCount(0);
        for(int i=0; i< getColumnCount(); ++i)
            addColumn(i);
    }

    public void setColumnNames(ArrayList<String> columnNames) {
        this.columnNames = columnNames;
    }
}
