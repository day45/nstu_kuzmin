import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by day45 on 31.01.16.
 */
public class CommandPanel extends JPanel {
    final public JComboBox<CommandsEnum> actionsBox;
    final private DbTableModel model;
    final private JLabel param1Label;
    final private JTextField param1Text;
    final private JLabel param2Label;
    final private JTextField param2Text;
    final private JLabel param3Label;
    final private JTextField param3Text;
    final private JButton button;

    public CommandPanel(DbTableModel tableModel) {
        super(new BorderLayout());

        this.model = tableModel;
        CommandsEnum.values();
        actionsBox = new JComboBox<>(CommandsEnum.values());
        actionsBox.setToolTipText(((CommandsEnum) actionsBox.getSelectedItem()).command);
        add(actionsBox, BorderLayout.NORTH);

        JPanel paramspan = new JPanel(new FlowLayout(FlowLayout.LEFT));

        param1Label = new JLabel(((CommandsEnum) actionsBox.getSelectedItem()).param1);
        paramspan.add(param1Label);

        param1Text = new JTextField();
        param1Text.setPreferredSize(new Dimension(100, 17));
        param1Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param1 != null);
        paramspan.add(param1Text);
        param2Label = new JLabel(((CommandsEnum) actionsBox.getSelectedItem()).param2);
        paramspan.add(param2Label);
        param2Text = new JTextField();
        param2Text.setPreferredSize(new Dimension(100, 17));
        param2Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param1 != null);
        paramspan.add(param2Text);
        param3Label = new JLabel(((CommandsEnum) actionsBox.getSelectedItem()).param3);
        paramspan.add(param3Label);
        param3Text = new JTextField();
        param3Text.setPreferredSize(new Dimension(100, 17));
        param3Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param1 != null);
        paramspan.add(param3Text);
        button = new JButton("Выполнить запрос.");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String command = ((CommandsEnum) actionsBox.getSelectedItem()).command;
                command = String.format(
                        "SELECT * from " + command + ";",
                        param1Text.getText(), param2Text.getText(), param3Text.getText()
                );
                System.out.println("Debug: " + command);
                try {
                    ArrayList<String> colName = new ArrayList<>();
                    model.setMatrix(SqlCommander.getMatrixRequest(command, colName));
                    model.setColumnNames(colName);
                    model.update();
                } catch (SQLException e) {
                    System.err.println("Error: " + e.getMessage());
                    JOptionPane.showMessageDialog(param1Label.getParent().getParent(),//todo: слишком железно
                            "Error: " + e.getMessage());
                }
            }
        });
        JPanel esheodnapanel = new JPanel(new BorderLayout());
        esheodnapanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        esheodnapanel.add(paramspan, BorderLayout.CENTER);
        esheodnapanel.add(button, BorderLayout.EAST);
        add(esheodnapanel, BorderLayout.CENTER);
        actionsBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                param1Label.setText(((CommandsEnum) actionsBox.getSelectedItem()).param1);
                param2Label.setText(((CommandsEnum) actionsBox.getSelectedItem()).param2);
                param3Label.setText(((CommandsEnum) actionsBox.getSelectedItem()).param3);
                param1Label.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param1 != null);
                param2Label.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param2 != null);
                param3Label.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param3 != null);
                param1Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param1 != null);
                param2Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param2 != null);
                param3Text.setVisible(((CommandsEnum) actionsBox.getSelectedItem()).param3 != null);
            }
        });
    }
}
