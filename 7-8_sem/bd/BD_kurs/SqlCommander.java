import java.sql.*;
import java.util.ArrayList;

/**
 * Created by day45 on 01.02.16.
 */
public class SqlCommander {

    private static Connection c;
    private static Statement stmt;

    public static void connect(String username, String password) throws SQLException {
        c = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/KURS", username, password);
        stmt = c.createStatement();
    }

    public static ArrayList<ArrayList<String>> getMatrixRequest(String sqlselect, ArrayList<String> colName) throws SQLException {

        ArrayList<ArrayList<String>> arrayList = new ArrayList<>();
        ArrayList<String> line;
        if (stmt == null) throw new SQLException("Not Connetion");
        ResultSet rs = stmt.executeQuery(sqlselect);
        while (rs.next()) {
            line = new ArrayList<>();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                if(arrayList.isEmpty())
                    colName.add(rs.getMetaData().getColumnName(i));
                line.add(rs.getArray(i).toString());
            }
            arrayList.add(line);
        }
        rs.close();
        return arrayList;
    }

    public static void close() throws SQLException {
        stmt.close();
        c.close();
    }
}
