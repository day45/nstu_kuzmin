/**
 * Created by day45 on 31.01.16.
 */
public enum CommandsEnum {
    afisha              (1, "afisha()",
            null, null, null, "киноафиша"),
    sort_film_by_date   (2, "sort_film_by_date()",
            null, null, null, "фильмы по дате проката"),
    sort_film_by_name   (3, "sort_film_by_name()",
            null, null, null, "в алфавитном порядке"),
    sort_film_by_cost   (4, "sort_film_by_cost()",
            null, null, null, "по стоимости билета"),
    films_by_date       (5, "films_by_date('%s')", "date",
            null, null, "по заданной дате определить какие фильмы идут в кинотеатрах. (2016.01.20)"),
    film_by_actor       (6, "film_by_actor('%s')",
            "text", null, null, "вывод фильмов по актеру(может быть несколько - таблица)"),
    rare_films_cinema   (7, "rare_films_cinema()",
            null, null, null, "доля редких фильмов(от общего числа фильмов для каждого кинотеатра и в целом."),
    rare_films          (8, "rare_films()",
            null, null, null, "доля редких в целом"),
    popular_actor       (9, "popular_actor()",
            null, null, null, "найти самого популярного актера(в макс фильмах)"),
    film_count          (10, "film_count('%s','%s','%s')",
            "varchar(64)", "timestamp", "timestamp", "сколько раз шел фильм за промежуток времени."),
    ticket_sum          (11, "ticket_sum()",
            null, null, null, "средняя цена билетов, макс и мин билет."),
    films_by_time       (12, "films_by_time('%s')",
            "timestamp", null, null, "по времени узнать начинающиеся сеансы"),
    actor_male_or_country(13, "actor_male_or_country('%s')",
            "varchar(32)", null, null, "по времени узнать начинающиеся сеансы"),
    actor_year_or_word  (14, "actor_year_or_word('%s', '%s')",
            "int", "text", null, "актеры мужчина или из \"страны\""),
    head_by_pay         (15, "films_by_time('%s', '%s')",
            "integer", "integer", null, "руководители, чей доход за (год) больше чем (число)"),
    pay_head_by_cinema  (16, "pay_head_by_cinema('%s','%s')",
            "integer(год)", "varchar(64)", null, "по названию киностудии - годовой доход призедента"),
    head_or_actor       (17, "head_or_actor('%s', '%s')",
            "страна varchar(32)", "имя varchar(32)", null, "актер или руководитель");

    final private Integer id;
    final public String command;
    final public String param1;
    final public String param2;
    final public String param3;
    final public String descr;

    CommandsEnum(Integer id, String command, String param1, String param2, String param3, String descr) {
        this.id = id;
        this.command = command;
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
        this.descr = descr;
    }

    @Override
    public String toString(){
        return descr;
    }
}
