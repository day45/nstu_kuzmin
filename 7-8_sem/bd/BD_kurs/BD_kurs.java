import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class BD_kurs {
    public static void main(String args[]) {
        try {
            String username = "analitic", password = "analitic";
            JFrame frame = new JFrame("Kuzmin BD. Username:" + username);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setMinimumSize(new Dimension(500, 300));
            frame.setPreferredSize(new Dimension(800, 450));
            JPanel centerPanel = new JPanel(new BorderLayout());
            centerPanel.setBorder(new EmptyBorder(12, 20, 12, 20));
            //таблица с результатами запроса
            DbTableModel tableModel = new DbTableModel();
            JScrollPane scrollPane = new JScrollPane(new JTable(tableModel));
            scrollPane.setBorder(new LineBorder(Color.gray, 1, true));
            centerPanel.add(scrollPane, BorderLayout.CENTER, 0);
            //панель запроса
            CommandPanel commandPanel = new CommandPanel(tableModel);
            centerPanel.add(commandPanel, BorderLayout.NORTH, 1);
            frame.add(centerPanel);
            //окно по центру
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
            int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
            frame.pack();
            frame.setLocation(x, y);
            frame.setVisible(true);
            SqlCommander.connect(username, password);
        } catch (Exception e) {
            StringBuilder errBuilder = new StringBuilder();
            for (StackTraceElement el : e.getStackTrace())
                errBuilder.append(el.toString()).append("\n");
            System.err.println("Error: " + e.toString() + errBuilder);
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage() + "\n Exit");
            System.exit(-1);
        }
    }
}