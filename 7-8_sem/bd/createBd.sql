CREATE TABLE CINEMA(
id 		SERIAL primary key not null,
name 	varchar(64)
);

CREATE TABLE FILM(
id 			SERIAL primary key not null,
id_actor 	int,
name 		varchar(64),
year 		int -- просто год
);


CREATE TABLE SHOW_TIME( --сеанс
id 			SERIAL primary key not null,
id_cinema 	int,
id_film 	int,
time 		timestamp, -- время 
cost		int,        --стоимость билета 
long        integer
);

CREATE TABLE ACTOR( -- в главных ролях
id 			SERIAL primary key not null,
name 		varchar(64),
country		varchar(32),
sex			varchar(10)
);

CREATE TABLE TICKET(
id 				SERIAL primary key not null,
id_show_time 	int,
place 			int,
hall  			int
);

CREATE TABLE WORKER( --сотрудники, руководители)
id 				SERIAL primary key not null,
id_cinema	 	int,
name 			varchar(64),
post  			varchar(64),
country		    varchar(32)
);

CREATE TABLE PAYMENT( --Выплаты ЗП
id 				SERIAL primary key not null,
id_worker	 	int,
sum 			int,
date 			timestamp -- время 
);