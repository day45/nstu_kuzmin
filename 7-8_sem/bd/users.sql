-- оператор - включение, чтение.
CREATE USER operator1 WITH password 'operator';
GRANT INSERT ON TABLE CINEMA, FILM, ACTOR, TICKET, WORKER, 
PAYMENT, SHOW_TIME  TO operator1;
GRANT SELECT ON TABLE CINEMA, FILM, ACTOR, TICKET, WORKER, 
PAYMENT, SHOW_TIME  TO operator1;

--пользователь, все изменения по строкам, но не больше.
CREATE USER kursuser WITH password 'kursuser';
GRANT ALL ON TABLE CINEMA, FILM, ACTOR, TICKET, WORKER, 
PAYMENT, SHOW_TIME  TO kursuser;

-- аналитик, чтение.
CREATE USER analitic WITH password 'analitic';
GRANT SELECT ON TABLE CINEMA, FILM, ACTOR, TICKET, WORKER, 
PAYMENT, SHOW_TIME  TO analitic;

-- администратор - все права по БД
CREATE USER adminkurs WITH password 'adminkurs';
GRANT ALL ON DATABASE "KURS" TO adminkurs;