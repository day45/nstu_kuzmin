--премьеры - все фильмы первый раз в кинотеатрах(вспомагательная)
CREATE OR REPLACE FUNCTION prem()
RETURNS TABLE(id_c integer, id_f integer, tim timestamp) AS $$
BEGIN
  RETURN QUERY 
  select id_cinema, id_film, min(time) 
  from show_time 
  group by id_cinema, id_film;
END;
$$ LANGUAGE plpgsql;
--1) киноафиша
CREATE OR REPLACE FUNCTION afisha()
RETURNS TABLE(cin varchar(64), fil varchar(64), dat timestamp, 
	           cos int, lon int, act varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name, min(SHOW_TIME.time), SHOW_TIME.cost, 
       SHOW_TIME.long, ACTOR.name --
  from SHOW_TIME, CINEMA, FILM, ACTOR 
  where ACTOR.id = FILM.id_actor and SHOW_TIME.id_cinema = CINEMA.id
      and SHOW_TIME.id_film = FILM.id

  group by CINEMA.name, FILM.name, SHOW_TIME.cost, 
       SHOW_TIME.long, ACTOR.name
  order by cinema.name;
END;
$$ LANGUAGE plpgsql;

--по дате проката:
CREATE OR REPLACE FUNCTION sort_film_by_date()
RETURNS TABLE(cin varchar(64), fil varchar(64), dat timestamp, 
	           cos int, lon int, act varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name, SHOW_TIME.time, SHOW_TIME.cost
  from SHOW_TIME, CINEMA, FILM
  where SHOW_TIME.id_cinema = CINEMA.id
      and SHOW_TIME.id_film = FILM.id
  order by cinema.name, SHOW_TIME.time;
  END;
$$ LANGUAGE plpgsql;
--в алфавитном порядке:
CREATE OR REPLACE FUNCTION sort_film_by_name()
RETURNS TABLE(cin varchar(64), fil varchar(64), dat timestamp, 
	           cos int, lon int, act varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name, SHOW_TIME.time, SHOW_TIME.cost
  from SHOW_TIME, CINEMA, FILM
  where SHOW_TIME.id_cinema = CINEMA.id
      and SHOW_TIME.id_film = FILM.id
  order by cinema.name, Film.name;
  END;
$$ LANGUAGE plpgsql;
--по стоимости билета:
CREATE OR REPLACE FUNCTION sort_film_by_cost()
RETURNS TABLE(cin varchar(64), fil varchar(64), dat timestamp, 
	           cos int, lon int, act varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name, SHOW_TIME.time, SHOW_TIME.cost
  from SHOW_TIME, CINEMA, FILM
  where SHOW_TIME.id_cinema = CINEMA.id
      and SHOW_TIME.id_film = FILM.id
  order by cinema.name, SHOW_TIME.cost;
END;
$$ LANGUAGE plpgsql;

-- по заданной дате определить какие фильмы идут в кинотеатрах. (2016.01.20)
CREATE OR REPLACE FUNCTION films_by_date(date)
RETURNS TABLE(cin varchar(64), fil varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name
  from SHOW_TIME, CINEMA, FILM
  where SHOW_TIME.id_cinema = CINEMA.id and SHOW_TIME.id_film = FILM.id 
        and SHOW_TIME.time::date = $1
  order by cinema.name;
END;
$$ LANGUAGE plpgsql;

--вывод фильмов по актеру(может быть несколько - таблица)
CREATE OR REPLACE FUNCTION film_by_actor(text)
RETURNS TABLE(films_by_actor varchar(64)) AS $$
DECLARE fil text;
BEGIN
  RETURN QUERY 
  select FILM.name
  from ACTOR, FILM
  where ACTOR.id = FILM.id_actor and ACTOR.name= $1;
END;
$$ LANGUAGE plpgsql;

--если бы фильм был один:


--доля редких фильмов(от общего числа фильмов для каждого кинотеатра и в целом.
--доля редких по кинотеатрам (отлажено. возвращает 0 так как такие значения в БД)
CREATE OR REPLACE FUNCTION rare_films_cinema()
RETURNS double precision AS $$
DECLARE count1 double precision;
DECLARE avgmy double precision;
DECLARE count2 double precision;
BEGIN
    --кол-во фильмов по кинотеатрам.
    select count(foo.count) into count1 from
    (select SHOW_TIME.id_cinema, SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_cinema, SHOW_TIME.id_film) as foo
    ;
    --среднее по кинотеатрам.
    select avg(foo.count) into avgmy from
    (select SHOW_TIME.id_cinema, SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_cinema, SHOW_TIME.id_film) as foo
    ;
	--кол-во тех, что меньше среднего
	select count(foo.count) into count2 from
    (select SHOW_TIME.id_cinema, SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_cinema, SHOW_TIME.id_film
    HAVING avgmy > 2 --count(SHOW_TIME.id_film)
    ) as foo;

	RETURN count2/count1;
END;
$$ LANGUAGE plpgsql;
--доля редких в целом
CREATE OR REPLACE FUNCTION rare_films()
RETURNS double precision AS $$
DECLARE count1 double precision;
DECLARE avgmy double precision;
DECLARE count2 double precision;
BEGIN
    select count(foo.count) into count1 from
    (select SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_film) as foo
    ;
    select avg(foo.count) into avgmy from
    (select SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_film) as foo
    ;
    select count(foo.count) into count2 from
    (select SHOW_TIME.id_film, count(SHOW_TIME.id_film) 
    from SHOW_TIME
    group by SHOW_TIME.id_film
    HAVING avgmy > 2 --count(SHOW_TIME.id_film)
    ) as foo;
    RETURN count2/count1;
END;
$$ LANGUAGE plpgsql;

--найти самого популярного актера(в макс фильмах). Может быть несколько.
CREATE OR REPLACE FUNCTION popular_actor()
RETURNS TABLE (name varchar(64)) AS $$
DECLARE num integer;
BEGIN
    --макс в скольки фильмах
    select max(foo.count) into num
    from (select ACTOR.name, count(ACTOR.name) 
    from ACTOR, FILM
    where FILM.id_actor = ACTOR.id
    group by ACTOR.name) as foo;
    --макс популярных актеров:
    RETURN QUERY 
    select ACTOR.name
    from ACTOR, FILM
    where FILM.id_actor = ACTOR.id
    group by ACTOR.name
    having count(ACTOR.name) = num;
END;
$$ LANGUAGE plpgsql;

--сколько раз шел фильм за промежуток времени.
--select film_count('1+1', '2016.1.15', '2016.11.11'); раньше - 2
CREATE OR REPLACE FUNCTION film_count(
    fil varchar(64), froma timestamp, toa timestamp
)
RETURNS integer AS $$
DECLARE num integer;
BEGIN
    select count(FILM.id) into num
    from FILM, SHOW_TIME
    where SHOW_TIME.id_film=FILM.id and
          SHOW_TIME.time < toa and
          SHOW_TIME.time > froma and
          FILM.name = fil;
    RETURN num;
END;
$$ LANGUAGE plpgsql;

--средняя цена билетов, макс и мин билет.
CREATE OR REPLACE FUNCTION ticket_sum()
RETURNS TABLE (avg_sum double precision, max_ticket_id integer, min_ticket_id integer ) AS $$
BEGIN
	--средняя цена
    select avg(SHOW_TIME.cost) into avg_sum
    from SHOW_TIME, TICKET
    where TICKET.id_show_time = SHOW_TIME.id;
    --max
    select TICKET.id into max_id
    from TICKET, SHOW_TIME
    where TICKET.id_show_time=SHOW_TIME.id
    ORDER BY SHOW_TIME.cost DESC LIMIT 1;
    --min
    select TICKET.id into min_id
    from TICKET, SHOW_TIME
    where TICKET.id_show_time=SHOW_TIME.id
    ORDER BY SHOW_TIME.cost LIMIT 1;
    RETURN NEXT;
    RETURN;
END;
$$ LANGUAGE plpgsql;

-- по времени узнать начинающиеся сеансы
CREATE OR REPLACE FUNCTION films_by_time(timestamp)
RETURNS TABLE(cin varchar(64), fil varchar(64)) AS $$
BEGIN
  RETURN QUERY 
  select CINEMA.name, FILM.name
  from SHOW_TIME, CINEMA, FILM
  where SHOW_TIME.id_cinema = CINEMA.id and SHOW_TIME.id_film = FILM.id 
        and SHOW_TIME.time = $1
  order by cinema.name;
END;
$$ LANGUAGE plpgsql;

--актеры мужчины или из "страны"
CREATE OR REPLACE FUNCTION actor_male_or_country(coun varchar(32))
RETURNS TABLE (name varchar(64)) AS $$
DECLARE num integer;
BEGIN
    RETURN QUERY select ACTOR.name from ACTOR
    where ACTOR.country = $1 or ACTOR.sex = 'male';
END;
$$ LANGUAGE plpgsql;
--снимался в фильме по году, либо название ф-а включает слово
CREATE OR REPLACE FUNCTION actor_year_or_word(yea int, wor text)
RETURNS TABLE (name varchar(64)) AS $$
DECLARE num integer;
BEGIN
    RETURN QUERY 
    select ACTOR.name from ACTOR, FILM
    where FILM.id_actor = ACTOR.id and
    (FILM.year = yea or position(wor in FILM.name) <> 0);
END;
$$ LANGUAGE plpgsql;

--руководители, чей доход за (год) больше чем (число)
CREATE OR REPLACE FUNCTION head_by_pay(yea integer, num integer)
RETURNS TABLE (name varchar(64), sum bigint) AS $$
BEGIN
    RETURN QUERY 
    select WORKER.name, sum(PAYMENT.sum) from WORKER, PAYMENT
    where WORKER.id = PAYMENT.id_worker and WORKER.post = 'head' and
        EXTRACT(YEAR FROM PAYMENT.date) = yea
    group by WORKER.name
    having sum(PAYMENT.sum) > num;
END;
$$ LANGUAGE plpgsql;

--по названию киностудии - годовой доход призедента. pay_head_by_cinema(2015, 'пионер');
CREATE OR REPLACE FUNCTION pay_head_by_cinema(yea integer, cin varchar(64))
RETURNS bigint AS $$
DECLARE num bigint;
BEGIN
    select sum(PAYMENT.sum) into num
    from WORKER, PAYMENT, CINEMA
    where WORKER.id = PAYMENT.id_worker and WORKER.post = 'head' and
        EXTRACT(YEAR FROM PAYMENT.date) = yea and
        WORKER.id_cinema = CINEMA.id and CINEMA.name = cin
    group by WORKER.name;
    RETURN num;
END;
$$ LANGUAGE plpgsql;

--KURS=# select head_or_actor('russia','Джейсон Стэйтем');
--KURS=# select head_or_actor('russia','Abram');
CREATE OR REPLACE FUNCTION head_or_actor(countr varchar(32), nam varchar(64))
RETURNS integer AS $$
DECLARE hea bigint;
DECLARE act bigint;
BEGIN
    select count(WORKER.id) into hea
	from WORKER where WORKER.country = countr and WORKER.name = nam;
    
    select count(ACTOR.id) into act
	from ACTOR where ACTOR.country = countr and ACTOR.name = nam;
    
    IF act != 0 THEN --если и то и другое тоже
     RETURN 1;
 	END IF;
 	IF hea != 0 THEN
     RETURN 2;
 	END IF;
    RETURN 3;
END;
$$ LANGUAGE plpgsql;

select count(WORKER.id) into num
from WORKER where WORKER.country = countr;