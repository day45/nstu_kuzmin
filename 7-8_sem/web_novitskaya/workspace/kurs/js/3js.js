function hex(d) {
   	return  ("0"+(Number(d).toString(16))).slice(-2).toUpperCase()
}
function rgb2hex(rgb) {
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  	));
  	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
	options = options || {};
	var expires = options.expires;

	if (typeof expires == "number" && expires) {
    	var d = new Date();
    	d.setTime(d.getTime() + expires * 1000);
    	expires = options.expires = d;
  	}
  	if (expires && expires.toUTCString) {
    	options.expires = expires.toUTCString();
  	}

  	value = encodeURIComponent(value);

  	var updatedCookie = name + "=" + value;

  	for (var propName in options) {
    	updatedCookie += "; " + propName;
    	var propValue = options[propName];
    	if (propValue !== true) {
      	updatedCookie += "=" + propValue;
    	}
  	}

  	document.cookie = updatedCookie;
}

$(document).ready(function(){
	var color = getCookie("b_col");
	if(color){
		$("body").css("background-color", color);
		$("#text_c_hex").text(color);
	}
	color = getCookie("t_col");
	if(color){
		$(".shadow_text").css("color", color);
		$("#back_c_hex").text(color);
	}

	$(".center_t th").mousedown(function(event){
		var color = $(this).css("background-color");
		color = rgb2hex(color);
		if(event.button == 0){
			//left
			$(".shadow_text").css("color", color);
			$("#text_c_hex").text(color);
			setCookie("t_col", color, null);
		} else if(event.button == 2){
			//right
			$("body").css("background-color", color);
			$("#back_c_hex").text(color);
			setCookie("b_col", color, null);
		}
	});
});
