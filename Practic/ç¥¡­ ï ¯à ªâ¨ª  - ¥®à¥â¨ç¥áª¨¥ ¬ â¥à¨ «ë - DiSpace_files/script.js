var tips = "", 
	 text_tip = "",
	 folder_style 	= 'public/css',
	 folder_js 		= 'public/js',
	 folder_images = 'public/images', 
	 workspace_id = $('#user_info').attr('workspace_id');
	 
if (!window.console){ console = {log: function() {}} };
	 
function updateTips(a, b) {
	if (b <= 0) {
		text_tip = text_tip + "<li>" + a + "</li>";
		tips.html(
				'<div id="errorMsg"><h3 class="at">Форма заполнена с ошибками. Исправьте, пожалуйста</h3><ol>'
						+ text_tip + "</ol></div>");
	} else {
		tips.html(a);
	}
}

function checkLength(c, a, b) {
	if ($.trim(c.val()).length == 0) {
		c.addClass("error");
		updateTips(a, b);
		return false
	} else {
		return true
	}
}

function checkId(c, err, d, a, b, e) {
	if (b == null) {
		b = 0
	}
	if ($.trim(c.val()) <= 0) {
		err.addClass("error");
		if (b == 0) {
			updateTips("Поле &laquo;" + d + "&raquo; не заполнено", 0)
		} else {
			updateTips(d, e)
		}
		return false
	} else {
		return true
	}
}



function checkMinLength(c, err, d, a, b, e) {
	if (b == null) {
		b = 0
	}
	if ($.trim(c.val()).length < a) {
		err.addClass("error");
		if (b == 0) {
			updateTips("Поле &laquo;" + d + "&raquo; должно быть более " + a
					+ " символов", 0)
		} else {
			updateTips(d, e)
		}
		return false
	} else {
		return true
	}
}

function checkAccLength(b, c, a) {
	if ($.trim(b.val()).length != a) {
		b.addClass("error");
		updateTips("Поле &laquo;" + c + "&raquo; должено быть ровно " + a
				+ " символов", 0);
		return false
	} else {
		return true
	}
}

function checkExpLength(b, c, a) {
	if (b.val().length != 0) {
		elm_array = b.val().split(";");
		if (elm_array.length < a) {
			b.addClass("error");
			updateTips("Поле &laquo;" + c + "&raquo; должно содержать хотя бы "
					+ a + " элемент(а/ов)", 0);
			return false
		}
	} else {
		b.addClass("error");
		updateTips("Поле &laquo;" + c + "&raquo; не было заполнено", 0);
		return false
	}
	return true
}

function checkRegexp(b, a, c) {
	if (!(a.test(b.val()))) {
		b.addClass("error");
		updateTips(c, 0);
		return false
	} else {
		return true
	}
}

function is_int( mixed_var ) {
  var y = parseInt(mixed_var * 1);
  if (isNaN(y)) return false;
  return mixed_var == y && mixed_var.toString() == y.toString();
}

function multiChange(elm){
	thisVal = $("#"+elm+"_all").val();
	if (thisVal != -1) {
		$("."+elm+" option[value='"+thisVal+"']").attr("selected",true);
	} 
}

function singleChange(elm){
	thisVal = $("."+elm).val();
	if (thisVal != -1) {
		$("#"+elm+"_all option:first").attr("selected",true);
	} 
}

function userChange(type){
  if($("#"+type+"").val() == 0) return false;
	if($(".u").is(':checked')){
		AskQuestion('Выполнить выбранное действие?', 
			function(){
				//$("#"+type+" option:first").attr("selected", true);
				
				$("#action").submit(); 
			}
		);
    return false;
    
   } else {
		alert("Для действия нужно отметить пользователей галочками.");
		$("#"+type+" option:first").attr("selected", true);
		return false;
   }
}

$(function() {

	if (document.getElementsByClassName) { // кроме IE8 и ниже 
		$(document.body).on('mouseenter', '.h', function(){
			$(this).addClass("highlight_row");
		});
		$(document.body).on('mouseleave', '.h', function(){
			$(this).removeClass("highlight_row");
		});
		
		// touchInit();
	}
	
	// simple menu
	$(document.body).on('click', '.open-more', function(e) {
    var opened = $(this).hasClass('open');
		$('.open-more').removeClass('open');
		$(this).toggleClass('open', !opened);
		
    if($('.simple-ui-layout').exists()) {
      $('.simple-ui-layout').remove();
    } else {
      $('<div/>', {'class': 'simple-ui-layout'})
        .on('click', function(){ 
          $('.open-more').removeClass('open'); 
          $(this).remove(); 
        })
        .appendTo('body');
    }
    
	});
	//$(document.body).on('click', ".simple-ui-menu ul.operations li a", function(e) {	console.log('A CLICKED'); $('.simple-ui-layout').remove();});
	//
  
	if( $( '#dialog-confirm' ).exists() ) {
	
		$( "#dialog-confirm" )
			.dialog({
				resizable: false,
				width: 400,
				modal: true,
				title: 'Уведомление',
				autoOpen: false,
				buttons: [
					{
						text: 'OK',
						click: function() {
						  $( this ).dialog( "close" );
						}
					},
					{
						text: 'Отмена',
						click: function() {
						  $( this ).dialog( "close" );
						}
					}
				],
				open: function() {
					$(this).parents('.ui-dialog-buttonpane button:eq(0)').focus(); 
				},
				close: function() {
					$('.simple-ui-layout').remove();
				}
			});
	}
	
});

/** 
 * Конвертация дат
 * Transform: 2011-11-11 11:11:11 => 11:11 11 ноя 2011 
 * dateformat=DM: Янв, Фев, Мар... // rus
 * dateformat=DMM: января, февраля, марта... // rus_month
 * dateformat=DMMY: января, февраля, марта... + год // rus_month_year
 * timeformat=HM : + в 10:00
 * timeformat=NO : no time appending
 */
function getDateFromUTC_JS(date, _dateformat, _timeformat)
{
	_dateformat = _dateformat || 'DM'; // rus
	_timeformat = _timeformat || 'HM';
	if(_dateformat!='DM'&&_dateformat!='DMMY'&&_dateformat!='DMM') return "Недоступно";
	var M_arr  = ["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"];
	var MM_arr = ["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"];
	data_arr = [];
	data_arr['DM']  = M_arr;
	data_arr['DMM'] = data_arr['DMMY'] = MM_arr;
	
	if (date) {
		var data = date.split(' ');
		var time = data[1].substr(0,5);	// 11:11
		var datata = data[0].split('-'); // 2011-11-11
		if (datata[1] <= 9 && datata[1].length == 2) datata[1] = datata[1].substr(1,1);
		
		month      = data_arr[_dateformat][datata[1]-1];
		month_full = data_arr['DMM'][datata[1]-1];
		
		year 			  = _dateformat=='DMM'?'':datata[0];
		year_full 	= datata[0];
		
		var DATE_FORMAT   = datata[2] + ' ' + month + ' ' + year;
		var DATE_FULL     = datata[2] + ' ' + month_full + ' ' + year_full;
    
    if(_timeformat == 'HM') {
      DATE_FORMAT += ' в ' + time;
      DATE_FULL   += ' в ' + time;
    }
		
		return '<span title="'+DATE_FULL+'">'+DATE_FORMAT+'</span>';
	}
	return "Недоступно";
}

/**

 * Сравнить полные даты
 UPD: Заменено на сравнение объектов Date();
		Проверить, что работает без этой функции и удалить мусор (27.09.2013)

function compare_date(start_day, start_hour, start_min, end_day, end_hour, end_min)
{
	var start_ = start_day.split('-');
	var end_ = end_day.split('-');
	if (start_[2] > end_[2]) return false;
	else {
		if (start_[2] == end_[2]) {
			if (start_[1] > end_[1]) return false;
			else {
				if (start_[1] == end_[1]) {
					if (start_[0] > end_[0]) return false;
					else {
						if (start_[0] == end_[0]) {
							if (parseFloat(start_hour) > parseFloat(end_hour)) return false;
							else {
								if (parseFloat(start_hour) == parseFloat(end_hour)) {
									if (parseFloat(start_min) > parseFloat(end_min)) return false;
									else {
										if (parseFloat(start_min) == parseFloat(end_min)) return false;
										else return true;
									}
								}
								else return true;
							}
						}
						else return true;
					}
				}
				else return true;
			}
		}
		else return true;
	}
}
*/ 

/**
 * Перезагрузить капчу
 */
function captchaReload (captchaImage, url) 
{
   var img = document.getElementById(captchaImage);
   if (img) img.src = url + Math.round(Math.random(0)*1000);
}

/**
 * Накинуть оверлей
 */
function createOverlay(msg)
{
  var msg_str = msg || 'Подождите, идет загрузка...';
  var docHeight = $(document).height();
  var href = document.location.href; // overlay_page_wait
  $('<div id="overlay_page"><div class="loading-dialog dh">'+ msg_str +'<br />\
		<div class="loading_long"></div> \
		<a href="'+href+'" onclick="deleteOverlay();"  class="close-overlay"><span class="ico ui-icon-closethick"></span></a>\
		</div>\
	</div>')
	  .appendTo("body")
	  .height(docHeight)
	  .css({
		 'position': 'absolute',
		 'top': 0,
		 'left': 0,
		 'background-color': 'rgba(255,255,255,0.5)',
		 'width': '100%',
		 'z-index': 1000000
	  });
  
  $(".loading-dialog").fadeIn();
  
}

/**
 * Снять оверлей
 */ 
function deleteOverlay()
{
	$('#overlay_page').fadeOut(500, function(){$(this).remove();});
}

/**
 * Функция подмены URL в адресной строке
 */ 
function set_url(url)
{
	if (decodeURI(window.location.href) != decodeURI(url))
		window.history.replaceState({}, "", url)
}


function getPositiveResult(val)
{
	return (val >= 0 ? val : 0)
}

// Return true if object exist
// USE: if( jQueryObj.exists() ) ...
jQuery.fn.exists = function() {
   return $(this).length;
}

function preg_quote( str ) {
	// http://kevin.vanzonneveld.net
	// +   original by: booeyOH
	// +   improved by: Ates Goral (http://magnetiq.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Onno Marsman
	// *     example 1: preg_quote("$40");
	// *     returns 1: '\$40'
	// *     example 2: preg_quote("*RRRING* Hello?");
	// *     returns 2: '\*RRRING\* Hello\?'
	// *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
	// *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
	return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
}

// задержка выполнения функции.
/*
Usage:
$('input').keyup(function() {
    delay(function(){
      alert('Time elapsed!');
    }, 1000 );
});
*/

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function setCookie (name, value, expires, path, domain, secure) {
	//expires это секунды!
	if (expires) {
		var date = new Date();
		var expiresday = date.setTime(date.getTime() + (expires*1000));
		expiresday = date.toGMTString();
	}
  document.cookie = name + "=" + escape(value) +
    ((expiresday) ? "; expires=" + expiresday : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function words_end(count, end0, end1, end2) {
	count100 = count % 100;
	count10  = count % 10;
	if (count100 >= 5 && count100 <= 20) return end0;
	if (count10 == 0) return end0;
	if (count10 == 1) return end1;
	if (count10 >= 2 && count10 <= 4) return end2;
	if (count10 >= 5 && count10 <= 9) return end0;
	else return end2;
}

function get_temp_pass(v_user_id) {

	$.get('/welcome/get_temp_pass', {
		user_id:  v_user_id
	},function(get) {
		if (get) {
			alert ('Запомните или запишите этот пароль: '+get);
		} else {
			alert ('Ошибка. Временный пароль не был сгенерирован.');
		}
	});
	return false;
}

function go2Page(url) 
{
	document.location.href = 'http://'+location.hostname+'/'+url;
  return false;
}

////////////////// rly need it?
function touchHandler(event) {
    var touch = event.changedTouches[0];

    var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent({
        touchstart: "mousedown",
        touchmove: "mousemove",
        touchend: "mouseup"
    }[event.type], true, true, window, 1, // TODO: уточнить координаты области действия
        touch.screenX, touch.screenY,
        touch.clientX, touch.clientY, false,
        false, false, false, 0, null);

    touch.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function touchInit() {
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);
}

// Загрузка счетчиков
function UpdateCounters() {
	if(workspace_id==300) return false; // magisters no ajax!
	$.post('/welcome/getev', { 
		// no params
	},function(response) {

		// Icon changing
		if(response.data.total && response.data.total>0 || response.data.privmsg && response.data.privmsg>0) 
			$('link[rel="icon"]').attr('href', folder_images+'/favicon_new.ico'); 
		else 
			$('link[rel="icon"]').attr('href', folder_images+'/favicon.ico');
		
		for(var counter in response.data) { 
			var num = response.data[counter]*1,
				old = $('#global-'+counter+'-counter').text()*1,
				num_html = num +'';
			
			$('body').trigger('counterEvent_'+counter, num); // throw trigger
			
			if(counter=='calendar') {
				if(num > 0) {
          num_html = num+' / '+response.data.calendar_passive;
        } else {
          num_html = response.data.calendar_passive;
        }
			}
			if(num > 0) {
				$('#global-'+counter+'-counter, #submenu-'+counter+'-counter').html( num_html ).addClass('new');
				if(num > old) 
					$('#global-'+counter+'-counter').removeClass('animated bounce').addClass('animated bounce');
			} else {
				$('#global-'+counter+'-counter, #submenu-'+counter+'-counter').html( '' ).removeClass('new');
			}
		}

	}, 'json');
}

function getsizetext(sizeinbytes) {
	if (sizeinbytes <= 0) 			 return 'неизвестно';
	if (sizeinbytes < 1024) 		 return 'менее 1 Кб';
	if (sizeinbytes < 1024*1024) return (sizeinbytes/1024).toFixed(1) +' Кб';
	else return (sizeinbytes/(1024*1024)).toFixed(1) +' Мб';
}

/**
 dinamicaly load JS file
 return: false if already included in this page (usually means error)
			true 	if tag exists, including not needed
			jquery_html_object if successfulty created.
*/
function loadJS(filename) {
	return loadJS_CSS('js', filename);
}

/**
 dinamicaly load CSS file
 return: false if already included in this page (usually means error)
			true 	if tag exists, including not needed
			jquery_html_object if successfulty created.
*/
function loadCSS(filename) {
	return loadJS_CSS('css', filename);
}

function loadJS_CSS(type, filename) {
	var src_folder 	= (type=='css'? folder_style : folder_js),
		 path_2_file 	=  src_folder + '/' + filename,
		 global_var   = filename.replace(/[\.\/\\\-]/g, ''),
		 tag = false;
	
	if(typeof window[global_var] !== 'undefined') return false;
	window[global_var] = 1;
	
	// console.log('including '+type+': '+filename+' ('+global_var+')...');
	
	if(type=='css') tag = $('link[href="' +path_2_file+'"]');
	else 				    tag = $('script[src="'+path_2_file+'"]');
	
	if(tag.exists()) return true;

	if(type=='css') {
		tag = document.createElement("link");
		tag.href = path_2_file;
		tag.type = 'text/css';
		tag.charset 	= 'utf-8';
		tag.rel		= 'stylesheet';
	} else if(type=='js') {
		tag = document.createElement("script");
		tag.src  = path_2_file; 
		tag.type = 'text/javascript';
	} else return false;
	
	return tag;
}

function AskQuestion( msg, okAction, cancelAction, title, ok_text, cancel_text) {
	title 		= title || 'Подтвердите действие';
	ok_text 	= ok_text || 'ОК';
	cancel_text  = cancel_text  || 'Отмена';
	cancelAction = cancelAction || function(){ return false; }
	
	if( $( '#dialog-confirm' ).exists() ) {
		$( '#dialog-confirm' ).find('.dialog-content').html( msg );
		
		$( '#dialog-confirm' )
			.dialog('option', 'title', title )
			.dialog('option', 'buttons', [
				{
					text: ok_text,
					click: function() {
					  $( this ).dialog( "close" );
						return okAction();
					}
				},
				{
					text: cancel_text,
					click: function() {
					  $( this ).dialog( "close" );
						return cancelAction();
					}
				}
			])
			.dialog('open');
	} else {
		// default confirm
		return confirm( msg.replace(/<br\/>/g, '\n' ) ) ? okAction() : cancelAction();
	}
}

function ip2long(IPaddress) {
  var ip = IPaddress.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
  if(ip) {
    return (+ip[1]<<24) + (+ip[2]<<16) + (+ip[3]<<8) + (+ip[4]);
  }
  return null;
}

var guid = (function() {
  function s4() {
	return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
  }
  return function() {
		 return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				  s4() + '-' + s4() + s4() + s4();
	  };
})();