$(document).ready(function() {

	var errorHeight = $('.errorMsg').height();
	$('.errorMsg .inner-title').width( $(window).width()-200 );
	
	$('.errorMsg').css({
		marginTop: -errorHeight*2,
		display: 'block',
		width:0
	}).delay(1000)
	.animate({
		marginTop: 0
	}, 500, function(){
		$(this).animate({
			width: '100%'
		}, 800, 'swing');
	});
	
	$('.noticeMsg')
		.css({
			top:'-200px',
			display:'block'
		})
		.delay(200)
			.animate({
				top:'0'
			}, 1300)
		.delay(8000)
			.animate({
				top: '-200px'
		}, 1300 ).delay(500).hide(200);
		
	// hiding
	$('.noticeMsg').on('click', function(){
		$(this).stop(true).slideUp(600);
	});
	
	$('.errorMsg').on('click', function(){
		$(this).stop(true).animate({width:0}, 600, function(){
			$(this).animate({
				marginTop: -errorHeight*2
			}, 500, function() {
				$(this).hide();
			});
		});
	});
	
	// ??
	$("#testDispaceMessage").on('click', function(){
		setCookie("test_read_js", "1", "7200", "/");
	});
	
	$(".open-info-page").on('click', function(){
		$('.info-page-content').slideDown();
	});
	
	// А есть такая?
	$('.closeNoticeButton').hover(function() {
		$(this).addClass("hover-fine-block");
	}, function() {
		$(this).removeClass("hover-fine-block");
	});
	
});