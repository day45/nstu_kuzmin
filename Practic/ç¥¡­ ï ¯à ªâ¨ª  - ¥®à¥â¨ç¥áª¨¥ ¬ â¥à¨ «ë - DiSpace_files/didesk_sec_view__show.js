/**
 * Скрипт 
 * © IDO 2013 - 2013
*/
	
$(function() {
	
	$('.link_to_anchor').on('click', function() {
		var anchor = $(this).attr("anchor");
		$('body,html').animate({
			'scrollTop':   $('#anchor_'+anchor).offset().top-$('#header').height()-6
		}, 500);
		$(".dd-section-header.selected").removeClass("selected");
		$("#header_"+anchor).addClass("selected");
	});
	
	modifyImages();
	
	$(window).resize(function() {
		modifyImages();
	});
	
	/*	
	$('.dd-infoblock-content').find('img').on('load', function(){
		$(this).fillImgWidth();
	});
	*/
	
});

function modifyImages()
{
	jQuery.each($('.dd-infoblock-content'), function(i, block) {
		var block_width = $(this).width();
		var images = $(block).find('img');
		images.each(function(){
			if( $(this).width() > block_width ) {
				$(this).css({
               "height": "auto",
               "width": "auto"
            });
			}
			$(this).css("max-width", block_width);
		});
	});

}