(function($) {
    $.fn.floating_panel = function(settings) {
        //значения по-умолчанию
        var config = {
            'fromLeft': 0,
            'fromTop': 0,
            'minTop': 0,
            'location': 'left'
        };
        //если пользователь указал свои параметры, то используем их
        if (settings) $.extend(config, settings);
 
        var element = $(this);
        
        var curWindow = $(window);
        //рассчитываем смещение от левого края окна браузера
        /*if ('left' == config.location) {
        	var elementLeft = config.fromLeft;
            //var elementLeft = (curWindow.width() / 2) - config.fromCenter;
        }
        else {
        	var elementLeft = config.fromLeft;
            //var elementLeft = curWindow.width() / 2 + config.fromCenter;
        }*/
        element.css({'left':config.fromLeft});
        updateElement();
        
        //изменяем положения виджета при прокрутке страницы 
        curWindow.scroll(function() {
            updateElement();
        });
        
        function updateElement() {
            //расстояние от начала страницы до верха её видимой части
            var windowTop = curWindow.scrollTop();
            if (windowTop + config.fromTop < config.minTop) {
                //виджет нужно позиционировать абсолютно
                if ('absolute' != element.css('position')) {
                    element.css('position', 'absolute');
                    element.css({'top':config.minTop});
                }
            } else {
                //позиционируем виджет фиксированно
                if ('fixed' != element.css('position')) {
                    element.css('position', 'fixed');
                    element.css({'top':config.fromTop});
                }
            }
        }
    };
})(jQuery);