function updateSubsectionPosition() {
	$('#dd-save-status').show();
	$('#dd-sec-actions, #dd-save-error').fadeOut(100);
	
	$.post('/didesk/course/proceed', {
		action: 'move_block',
    sec_id: SECTION_ID,
		sub_pos_arr:  $('#sortable').sortable('toArray')+""
	},function(get) {
		$('#dd-save-status').hide();
		if (get == '0') {
			$('#dd-save-error').html('<span class="color-red">Ошибка сохранения порядка ('+get+')</span>').show();
		} else {
			$('#dd-sec-actions').fadeIn(600);
		}
	});

}


$(function() {
	$("#sortable").disableSelection();
	$("#sortable").sortable({
		placeholder: "sortable-state-highlight",
		handle: '.dd-section-header .head-title',
		addClasses: false,
		revert: 200,
		opacity: 0.7,
		start: function(event, ui) {
			$('.sortable-state-highlight').height( $(ui.item).height()-1 );
			$('.ui-sortable').css('cursor', '-moz-grabbing');
		},
		stop: function(event, ui) {
			$('.ui-sortable').css('cursor', '-moz-grab');
		},
		update: function(event, ui) { 
			updateSubsectionPosition(); //sortable li span.order_no a
		}
	});
	
	$( ".dd-menu-element.item-droppable" ).droppable({
		activeClass: "dd-menu-drop-active",
		hoverClass:  "dd-menu-drop-highlight",
		accept: ".dd-block-draggable",
		tolerance: 'pointer',
		drop: function( event, ui ) {
			var block_id = ui.helper.attr('id'),
				 sec_id   = $(this).attr('sec_id'),
				 block_title = ui.helper.find('.head-title').text(),
				 sec_name = $(this).find('a').text();
				 
			if(block_title) block_title = '&laquo;'+block_title+'&raquo; ';
			
			if(block_id>0 && sec_id>0) { 
				
				AskQuestion('Переместить блок '+block_title+'<br/>в раздел <b>'+sec_name+'</b>?',
					function(){
						ui.helper.hide(1000);
						$.post('/didesk/course/proceed', {
							action: 'move_block2sec',
							block_id: block_id,
							sec_id: sec_id
						},function(get) {
							if (get != '0') {
								alert('Ошибка перемещения: '+get);
							} else {
								$('#ok_status').show();
							}
						});
					}, false,
					'Перемещение блока',
					'Да, переместить'
				);

			}
		}
	});
	
	$(".js-edit-infoblock").hover(
		function() {$(this).closest("li").find(".DD_sectionContent").addClass("hover");},
		function() {$(this).closest("li").find(".DD_sectionContent").removeClass("hover");}	
	);
	
	$(".js-delete-infoblock").hover(
		function() {$(this).closest("li").find(".DD_sectionContent").addClass("delete");},
		function() {$(this).closest("li").find(".DD_sectionContent").removeClass("delete");}	
	);
	
	$(".delete_all_link").hover( //.DD_menuElementSelected, 
		function() {$("#section-content").addClass("delete");},
		function() {$("#section-content").removeClass("delete");}	
	);
	
	$(".file_deny").hover(
		function() {$("#DD_PassBlock").addClass("deactiv");},
		function() {$("#DD_PassBlock").removeClass("deactiv");}	
	);
	
	// docx import block
	$(".btn_choose_docx").on('click', function(){
		$('#file_docx').click(); return false;
	});
	$("#file_docx").on('change', function(){
		$('#form_docx').submit();
	});

});


function hideOpenSection(sec_id, old_status, new_status) {
	$.post('/didesk/course/proceed', {
		action: 'hide_section',
		sec_id: sec_id,
		status: new_status,
	},function(get) {
		if (get.error != '' && get.error != undefined) {
			alert ('Ошибка скрытия раздела: '+get.error);
		} else {
			
			$('.'+new_status+'_part').hide();
			$('.'+old_status+'_part').show();
			$("#section-content, #menu_"+sec_id).removeClass( old_status ).addClass( new_status );
		
		}
	});
}

function deleteSection(sec_id) {
	var next_id = (title_id>0) ? title_id-1 : '';
	AskQuestion('Этот <b>раздел будет удалён</b> со всеми информационными блоками! Т.е. исчезнет не только раздел, но и информационные блоки из него.', 
		function(){
			$.post('/didesk/course/proceed', {
				action: 'delete_section',
				sec_id: sec_id
			},function(get) {
				if (get.error != '' && get.error != undefined) {
					alert ('Ошибка удаления: '+get.error);
				} else {
					$( "#sortable li" ).slideUp('normal');
					setTimeout('location.replace("didesk/course/show/'+curs_id+'/'+next_id+'")', 900);
				}
			});
		},
		false,
		'Удаление раздела',
		'Да, удалить этот раздел'
	);
}

function deleteInfoblock(sub_id) {
	AskQuestion('Этот информационный блок будет удалён.', 
		function(){
			$.post('/didesk/course/proceed', {
				action: 'delete_infoblock',
				curs_id: curs_id,
				sub_id: sub_id
			},function(get) {
				if (get == 'success') {
					$( '#'+ sub_id ).slideUp('normal', function() {
            $(this).remove();
            // if last infoblock - refresh page
            infoblocksCount = $('.dd-infoblock').length;
            if(infoblocksCount <= 0) {
              location.replace('didesk/course/show/'+ curs_id +'/'+ title_id +'');
            }
          });
          
         
				} else {
					alert ('Ошибка удаления: '+get.error);
				}
			});
		},
		false,
		'Удаление информационного блока',
		'Да, удалить блок'
	);
}

function copyInfoblock(sub_id) {
	AskQuestion('Будет создана копия информационного блока.', 
		function(){
			$.post('/didesk/course/proceed', { 
				action: 'copy_infoblock',
				curs_id: curs_id,
				sub_id: sub_id
			},function(get) {
				if (get == 'success') {
					alert('Копия успешно создана и помещена в конце раздела. \r\rСейчас страница будет обновлена.');
					location.replace('didesk/course/show/'+ curs_id +'/'+ title_id +'');
				} else {
					alert ('Ошибка копирования: '+get);
				}
			});
		},
		false,
		'Создать копию курса',
		'Да, создать копию'
	);
}