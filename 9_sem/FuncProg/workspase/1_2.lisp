;Определить функцию, вычисляющую, сколько всего 
;атомов в списке (списочной структуре)

; длина списка
(defun len (list)
    (cond ((eq list nil) 0)
        (t (+ 1 (len (cdr list))))
    )
)

; рекурсивно для дерева(для 2ой лабы)
(defun node_counter (L &OPTIONAL (N 0))
  (cond
    ((null L) N)
	((listp (car L)) (+ (node_counter  (car L)) (node_counter  (cdr L) N)))
	(T (node_counter  (cdr L) (+ 1 N)) )))

(print(len '(1 2 3 4 '(5 5 5) ) ))