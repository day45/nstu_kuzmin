; Имеется ассоциативный список, вторым элементом точечных пар которого 
; является символ. Организовать формирование списка свойств символа, 
; определяемого по заданному ключу. Ввод имен свойств и их значений 
; выполнить с клавиатуры. Организовать вывод ассоциативного списка 
; и списка свойств его элементов в наглядном виде.

; Глобальный ассоциативный спискок
(setq x '((1 . apple) (2 . potato) (3 . orange)))

; Получить список свойств по ключу.
(defun props_by_key (key a-list)
  (setq res (cdr (assoc key a-list)))
  (if res
    (symbol-plist res)
  )
)

; Печать одной пары и свойств значения
(defun print_pair (pair)
  (print pair)
  (print (symbol-plist (cdr pair)))
)

; Печать а списка
(defun print_a_list (tree)
  (cond ((null tree) ())
    (T (append (list (print_pair(car tree))) (print_a_list (cdr tree))))
  )
)

;Ввод имени свойства и значения с клавиатуры.
(defun do-set-prop ()
  (print "Введите символ: ")
  (setq sym (READ))
  (print "Введите имя свойства: ")
  (setq sv-name (READ))
  (print "Введите значение свойства: ")
  (setq sv-value (READ))
  (setf ( get sym sv-name ) sv-value)
)

;Меню для ввода свойств и печати списка.
(defun uni-menu (lst-name)
 (loop 
   (let ((p 0) (c 0))  
      (dolist (m lst-name nil)
        (print m)
        (setq p (1+ p))
      )
      (print "")
      (print "Ваш выбор: ")
      (setq c (read))
      (if (= c 0) 
        (return 'ok)
      (if (= c 1) 
        (do-set-prop)
      (if (= c 2) 
        (print_a_list x)
      (print "Нет такого пункта меню"))))
      
      (print "")
    )
  )
)

(uni-menu '("0) Завершить." "1) Добавить свойство." "2) Печать а-списка."))