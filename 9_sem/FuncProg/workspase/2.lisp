;Имеется список, элементы которого — непустые бинарные деревья. 
;Для каждого элемента списка найти среднее арифметическое 
;листьевых вершин, из полученных результатов сформировать 
;список. Выполнить реверс для полученного списка.


; сумма всего дерева
(defun sum-num (lst)
  (cond ((null lst) 0)
        ((listp (car lst)) (+ (sum-num (car lst)) (sum-num (cdr lst))))
        ((numberp (car lst)) (+ (car lst) (sum-num (cdr lst))))
        (t (sum-num (cdr lst)))))

; длина всего дерева
(defun node_counter (L &OPTIONAL (N 0))
  (cond
    ((null L) N)
	((listp (car L)) (+ (node_counter  (car L)) (node_counter  (cdr L) N)))
	(T (node_counter  (cdr L) (+ 1 N)) )))

; среднее арифметическое всего дерева
(defun average (list)
  ( / (sum-num list ) (node_counter list) )
)

; обход списка деревьев
(defun depth_list (tree)
	(cond ((null tree) ())
		(T (append (list (average(car tree))) (depth_list (cdr tree))))))

(defun lab2 (tree) 
	(reverse(depth_list tree ))
)

(print(lab2 '((1 1 1) (2 2 2) (3 3 3) (5 5 5) ) ))