;Имеется список чисел, определить функционал, 
;возвращающий список квадратов/кубов чисел.

(defun pf (x f) (cond ((null x) nil)
    (T (cons (funcall f (car x)) (pf (cdr x) f))))
)
					  
(defun square (x) (expt x 2)) 
(defun cube (x) (expt x 3)) 

(print (pf '(1 2 3 4 5) 'square) )
(print (pf '(1 2 3 4 5) 'cube) )