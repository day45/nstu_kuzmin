; Определить рекурсивную функцию, возвращающую значение n-го члена ряда Фибоначчи
(defun fibonacci_req (n &optional (a 0) (b 1) (acc ()))
  (if (zerop n)
      (nreverse acc)
      (fibonacci_req (1- n) b (+ a b) (cons a acc))))

(defun fibonacci (n)
	 (nth (- n 1) (fibonacci_req n))
)

(print (fibonacci 10))