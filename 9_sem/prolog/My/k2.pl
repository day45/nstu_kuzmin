gazetteer("russia","7500").
gazetteer("elena","7500").
gazetteer("everest","11000").
gazetteer("more","3400").

all:-
	gazetteer(X,Y),write(X),write("-"),write(Y),nl,fail.

byname(X):-
	gazetteer(X,Y),write(X),write("-"),write(Y),nl,fail.

byvalue(Y):-
	gazetteer(X,Y),write(X),write("-"),write(Y),nl,fail.

gazetteermenu:-
	repeat,
		write("1-Печать справочника"),nl,
		write("2-Поиск по названию"),nl,
		write("3-Поиск по значению"),nl,
		write("0-Выход"),nl,nl,
		read(N),do(N).
			do(1):-all().
			do(2):-write("Название"),read(X),byname(X).
			do(3):-write("Значение"),read(X),byvalue(X).
			do(0).