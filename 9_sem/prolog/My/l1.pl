inf():-inf().

%% фибоначи
fib(0, 0):-!.
fib(1, 1):-!.
fib(N, X):-
  N1 is N-1,
  N2 is N-2,
  fib(N1, X1),
  fib(N2, X2),
  X is X1+X2.


tailfib(0, 0).
tailfib(1, 1).
tailfib(N,Result):-fibonacci(N,0,1,Result).

fibonacci(0,N,_,N).
fibonacci(N, Prev1,Prev2,Result):-
   N>0,
   New_Prev2 is Prev1+Prev2,
   N1 is N-1,
   fibonacci(N1,Prev2,New_Prev2,Result).

%% nый член списка
getn([],N,_) :- write("big num").

getn([H|_],0,H) :-
    !.

getn([_|T],N,H) :-
    N > 0,
    N1 is N-1,
    getn(T,N1,H).

%% ?- getn([1,2,3],2,Res).