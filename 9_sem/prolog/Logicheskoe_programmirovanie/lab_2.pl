subscriber("Ivan",12345).
subscriber("Sasha",57567).
subscriber("Semen",40554).
subscriber("Masha",12345).
subscriber("Fillipp",89644).
subscriber("Vera",66677).

menu:-repeat,nl,write("1-Âñÿ èíôîðìàöèÿ"),nl,
write("2-Ïîèñê ïî òåëåôîíó"),nl,
write("3-Ïîèñê ïî èìåíè"),nl,
write("0-exit"),nl,nl,
read(N),do(N),N=0.

do(1):-subscriber(X,Y),write(X),write(" "),write(Y),write(" "),nl.

do(2):-!,write("Òåëåôîí"),
read(Y),subscriber(X,Y),
write(X),write(" "),nl,fail.

do(3):-!,write("Èìÿ"),
read(X),subscriber(X,Y),
write(Y),write(" "),nl,fail.

do(0).
rep.
rep:-repeat.

pow(_,0,1):-!.
pow(X,Y,Z):- NY is Y-1,pow(X,NY,NZ),Z is NZ*X.
hvostpow(_,X,1,X):-!.
hvostpow(NX,X,Y,Z):- NY is Y-1,NZ is X*NX,hvostpow(NX,NZ,NY,Z).
hvostpow(X,Y,Z):-hvostpow(X,X,Y,Z).