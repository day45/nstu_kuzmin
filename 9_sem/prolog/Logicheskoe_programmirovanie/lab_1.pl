parents(fillipp, ivan).
parents(fillipp, vitya).
parents(fillipp, sasha).
parents(vera, ivan).
parents(vera, vitya).
parents(vera, sasha).

parents(ivan, tolya).
parents(ivan, lena).
parents(ivan, lisa).
parents(ivan, vika).
parents(maria, tolya).
parents(maria, lena).
parents(maria, lisa).
parents(maria, vika).

parents(vitya, misha).
parents(vitya, dasha).
parents(sveta, misha).
parents(sveta, dasha).

parents(sasha, kira).
parents(ksyusha, kira).

man(ivan).
man(vitya).
man(tolya).
man(sasha).
man(misha).
man(fillipp).

woman(vera).
woman(dasha).
woman(maria).
woman(sveta).
woman(lena).
woman(lisa).
woman(vika).
woman(ksyusha).
woman(kira).

son(X,Y):-parents(Y,X), man(X).
daughter(X,Y):-parents(Y,X), woman(X).
mother(X,Y):-parents(X,Y), woman(X).
father(X,Y):-parents(X,Y), man(X).
sister(X,Y):-parents(Z,X), parents(Z,Y), woman(X).
brother(X,Y):-parents(Z,X), parents(Z,Y), man(X).
cousin(X,Y):-parents(A,Y), parents(B,X), parents(C,A), parents(C,B).
grandfather(X,Y):-parents(X,Z),parents(Z,Y), man(X).
