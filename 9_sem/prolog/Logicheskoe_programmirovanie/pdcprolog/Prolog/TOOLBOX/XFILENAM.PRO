/****************************************************************

     Turbo Prolog Toolbox
     (C) Copyright 1987 Borland International.

		Demo of	readfilename utility
****************************************************************/

include "tdoms.pro"

DATABASE
  insmode
  lineinpstate(STRING,COL)
  lineinpflag

include "tpreds.pro"
include "lineinp.pro"
include "filename.pro"
include "status.pro"

GOAL
	makewindow(2,66,7,"Instructions",0,0,24,40),
	disk(DIR),
	nl,write(" 1. Set the directory\n    to your tools dir:\n    ",DIR),nl,nl,
	makewindow(3,36,7,"Results",0,40,24,40),
	makestatus(112," Give some filenames and inspect the result of readfilename."),
	setdir(5,5,23,23),disk(DIR),write("Directory=",DIR),nl,nl,
	shiftwindow(2),
	write(" Now try the following in turn:"),nl,
	write(" =============================="),nl,nl,
	write("  2. Just press RETURN"),nl,
	write("  3. Give the filename \"test\""),nl,
	write("  4. Try the filename \"test.txt\""),nl,
	write("  5. Delete the old name and\n     then press RETURN"),nl,nl,nl,nl,
	write(" - Ctrl-backspace deletes the\n   old filename"),
	write("\n\n\n - Use Ctrl-Break to stop"),
	shiftwindow(3),
	changestatus(" Give some filenames and inspect the result of readfilename."),
	repeat,
	refreshstatus,
	readfilename(19,41,23,20,pro,"oldname.dat",NEWNAME),
	write("Filename=",NEWNAME),nl,
	fail.
