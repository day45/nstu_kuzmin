/*******************************************************************

     Turbo Prolog Toolbox
     (C) Copyright 1987 Borland International.

		Demo of screen handler
	Simple adding machine with memory facility
*******************************************************************/

code=2000

include "tdoms.pro"

DOMAINS
  FNAME=SYMBOL
  TYPE = int(); str(); real()

DATABASE
  /* Database declarations used in scrhnd */
  insmode			/* Global insertmode */
  actfield(FNAME)		/* Actual field */
  screen(SYMBOL,DBASEDOM)	/* Saving different screens */
  value(FNAME,STRING)		/* value of a field */
  field(FNAME,TYPE,ROW,COL,LEN) /* Screen definition */
  txtfield(ROW,COL,LEN,STRING)
  windowsize(ROW,COL).
  notopline

  /* Database declarations used in vscrhnd */
  windowstart(ROW,COL)
  mycursord(ROW,COL)

  /* Database declarations used in lineinp */
  lineinpstate(STRING,COL)
  lineinpflag

include "tpreds.pro"
include "menu.pro"
include "status.pro"
include "lineinp.pro"
include "scrhnd.pro"

GOAL
	consult("xmadder.scr"),
	createwindow(off),
	scrhnd(off,_).

CLAUSES
/*******************************************************************
			Field action
*******************************************************************/

  field_action("Total"):-
	retract(value("Memory",_)),fail.

  field_action("Total"):-
	field_value("Total",T),
	assert(value("Memory",T)).
  
/*******************************************************************
			 Field_value
*******************************************************************/

  field_value("Total",T):-
	value("FirstNo",N1),
	str_real(N1,Num1),
	value("SecondNo",N2),
	str_real(N2,Num2),!,
	Tnum = Num1+Num2,
	str_real(T,Tnum).

  field_value("Total",N1):-
	value("FirstNo",N1),!.

  field_value("Total",N2):-
	value("SecondNo",N2),!.

  field_value("Total","0"):-!.

  field_value(FNAME,VAL):-
	value(FNAME,VAL),!.  

/*******************************************************************
			 noinput
*******************************************************************/

  noinput("Memory").
  noinput("Total").
