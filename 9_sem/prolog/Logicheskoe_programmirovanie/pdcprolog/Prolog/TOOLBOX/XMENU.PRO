/****************************************************************

     Turbo Prolog Toolbox
     (C) Copyright 1987 Borland International.

		Demo of	menu
****************************************************************/

include "tdoms.pro"
include "tpreds.pro"
include "status.pro"
include "menu.pro"

Predicates
  test
  p(INTEGER)

CLAUSES
  test:-
	changestatus("Normal menu    Cr:Select    Esc:abort"),
	menu(1,1,112,66,[first,second,third,fourth],"Normal menu",3,CHOICE),p(CHOICE),
	write("You chose: ",CHOICE),nl,fail.
	
  test:-
	changestatus("Normal menu with specified initial selection   Cr:Select    Esc:abort"),
	menu(10,10,7,66,[first,second,third,fourth],"Normal menu",3,CHOICE),
	write("You chose: ",CHOICE),nl,fail.

  test:-
	changestatus("Use of menu without a frame    Cr:Select    Esc:abort"),
	menu(10,10,112,0,[first,second,third,fourth],"No frame",2,CHOICE),
	write("You chose: ",CHOICE),nl,fail.

  test:-
	changestatus("Use of menu_leave    Cr:Select    Esc:abort"),
	menu_leave(5,0,7,66,[first,second,third,fourth],first,1,CH1),
	menu_leave(10,20,7,71,[first,second,third,fourth],second,2,CH2),
	menu_leave(15,40,7,36,[first,second,third,fourth],third,3,CH3),
	removewindow,removewindow,removewindow,
	write("You chose: ",CH1,", ",CH2,", ",CH3),nl,fail.

  test:-
	changestatus("Use of menu_mult    F10: End   Cr:Select or delete   Esc:abort"),
	menu_mult(10,10,7,66,[first,second,third,fourth,fifth,"6","7"],"test",[3,6],CHOICEL),
	write("You chose: ",CHOICEL),nl,fail.

  test.
p(1):-write("INNA ANAT"),readchar(_).

  GOAL
	makewindow(1,23,0,"test",0,0,24,80),
	makestatus(112,"Choose a number"),
	test,
	changestatus("End of demo").
