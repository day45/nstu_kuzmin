var Ymax = 3; //MAX # OF PIXEL STEPS IN THE "X" DIRECTION
var Xmax = 3; //MAX # OF PIXEL STEPS IN THE "Y" DIRECTION
var Tmax = 10000; //MAX # OF MILLISECONDS BETWEEN PARAMETER CHANGES

var NS4 = (navigator.appName.indexOf("Netscape") >= 0 && parseFloat(navigator.appVersion) >= 4 && parseFloat(navigator.appVersion) < 5) ? true : false;
var IE4 = (document.all) ? true : false;
var NS6 = (parseFloat(navigator.appVersion) >= 5 && navigator.appName.indexOf("Netscape") >= 0) ? true : false;

var wind_w, wind_h;
var GNUM;
var Ydir, Xdir, Ystep, Xstep;

function moveimage() {
	if (getidleft() + getidwidth() + Xstep  >= wind_w + getscrollx()) Xdir = false;
	if (getidleft() - Xstep <= getscrollx()) Xdir = true;
	if (getidtop() + getidheight() +Ystep >= wind_h + getscrolly()) Ydir = false;
	if (getidtop() - Ystep <= getscrolly()) Ydir = true;
	moveidby((Xdir) ? Xstep : -Xstep, (Ydir) ? Ystep : -Ystep);
}

function getnewprops() {
	Ydir = Math.floor(Math.random() * 2) > 0;
	Xdir = Math.floor(Math.random() * 2) > 0;
	Ystep = Math.ceil(Math.random() * Ymax);
	Xstep = Math.ceil(Math.random() * Xmax)
	setTimeout('getnewprops()', Math.floor(Math.random() * Tmax));
}

function getscrollx() {
	if (NS4 || NS6) return window.pageXOffset;
	if (IE4) return document.body.scrollLeft;
}

function getscrolly() {
	if (NS4 || NS6) return window.pageYOffset;
	if (IE4) return document.body.scrollTop;
}

function getid(name) {
	if (NS4) return document.layers[name];
	if (IE4) return document.all[name];
	if (NS6) return document.getElementById(name);
}

function moveidto(x, y) {
	GNUM.style.left = x + 'px';
	GNUM.style.top = y + 'px';
}

function getidleft() {
	return parseInt(GNUM.style.left);
}

function getidtop() {
	return parseInt(GNUM.style.top);
}

function getidwidth() {
	return parseInt(GNUM.clientWidth);
}

function getidheight() {
	return parseInt(GNUM.clientHeight);
}

var cur_num = 0;
var iter = 0;

function moveidby(dx, dy) {
	GNUM.style.left = (getidleft() + dx) + 'px';
	GNUM.style.top = (getidtop() + dy) + 'px';

	if(iter++ == 30){
		var asd = $('#gnum_' + cur_num);
		asd.css("display", "block");
		$('#gnum_' + (cur_num-1)).css("display", "none");
		cur_num +=1;
		if(cur_num > MAX_num_idx+1){
			cur_num = 0;
			$('#gnum_' + (MAX_num_idx+1)).css("display", "none");
		}
		iter = 0;
	}
}

function getwindowwidth() {
	return document.documentElement.clientWidth;
}

function getwindowheight() {
	return document.documentElement.clientHeight;
}

function init() {
	wind_w = getwindowwidth();
	wind_h = getwindowheight();
	GNUM = getid('gnum');
	getnewprops();
	moveidto(Math.floor(Math.random() * (wind_w - GNUM.style.top)), Math.floor(Math.random() * (wind_h - GNUM.style.left)));
	if (NS4) GNUM.visibility = "show";
	if (IE4 || NS6) GNUM.style.visibility = "visible";
	startfly = setInterval('moveimage()', Math.floor(Math.random() * 30) + 30);
}

if (NS4 || NS6 || IE4) {
	window.onload = init;
	window.onresize = function() {
		wind_w = getwindowwidth();
		wind_h = getwindowheight();
	}
}