/**
 * Класс для храниения представления символов в виде таблицы булевых значений
 */
public class CharData implements Comparable, Cloneable {

    //символ
    private char character;
    //его таблица
    private boolean[][] table;

    public CharData(char character, int width, int height) {
        table = new boolean[width][height];
        this.character = character;
    }

    public void setData(int x, int y, boolean v) {
        table[x][y] = v;
    }

    //Получить числовое значение строки от -0,5 до 0,5
    public double getDoubleRow(int y) {
        int result = 0;
        for(int x=0; x < getWidth(); ++x){
            if(table[x][y]) result += Math.pow(2, x);
        }
        //System.out.println(String.format("%c - %f", character, (double)result / 127. - .5));
        return (double)result / 127. - .5;
    }

    public boolean getData(int x, int y) {
        return table[x][y];
    }

    public void clear() {
        for (int x = 0; x < table.length; x++)
            for (int y = 0; y < table[0].length; y++)
                table[x][y] = false;
    }

    public int getHeight() {
        return table[0].length;
    }

    public int getWidth() {
        return table.length;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof CharData) {
            CharData obj = (CharData) o;
            if (this.getCharacter() > obj.getCharacter())
                return 1;
            else
                return -1;
        } else throw new ClassCastException(o == null ? "null" : o.toString());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CharData && character == ((CharData) obj).getCharacter();
    }

    @Override
    public int hashCode() {
        return Character.hashCode(character);
    }

    @Override
    public String toString() {
        return "" + character;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        CharData obj = (CharData) super.clone();
        obj.table = new boolean[getWidth()][getHeight()];
        obj.character = character;

        for (int y = 0; y < getHeight(); y++)
            for (int x = 0; x < getWidth(); x++)
                obj.setData(x, y, getData(x, y));
        return obj;
    }
}