import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Реализация нейронной сети Кохонена для распознавания вводимых символов.
 * Имеет входной слой равный кол-ву значемых пикселей. И выходной - равный кол-ву букв.
 * <p>
 * ЗФ-222.
 */

public class Main extends JFrame implements Runnable {

    /** Файл с эталоными данными */
    private static final String DATA_FILE = "reference.dat";
    /** Файл с поврежденными данными */
    private static final String BROKEN_DATA_FILE = "broken.dat";

    /** The downsample width for the application.*/
    private static final int DOWNSAMPLE_WIDTH = 7;

    /** The down charDataPanel height for the application.*/
    private static final int DOWNSAMPLE_HEIGHT = 8;

    /**
     * The down charDataPanel component to display the drawing
     * downsampled.
     */
    private CharDataPanel charDataPanel;

    /** Модель с эталонными данными */
    private DefaultListModel referenceListModel = new DefaultListModel();
    /** Модель с поврежденными данными */
    private DefaultListModel brokenListModel = new DefaultListModel();

    /**
     * The neural network.
     */
    private KohonenNetwork net;

    /**
     * The background thread used for training.
     */
    private Thread trainThread = null;

    /**
     * The constructor.
     */
    public Main() {
        JLabel label;

        getContentPane().setLayout(null);
        charDataPanel = new CharDataPanel(DOWNSAMPLE_WIDTH, DOWNSAMPLE_HEIGHT);
        charDataPanel.setBounds(240, 180, 65, 48);
        getContentPane().add(charDataPanel);

        //{{INIT_CONTROLS
        setTitle("Kohonen Neural src.network.Network");
        getContentPane().setLayout(null);
        setSize(325, 260);
        setVisible(false);
        label = new JLabel("Letters");
        getContentPane().add(label);
        label.setBounds(17, 12, 84, 12);
        test.setText("Test");
        test.setActionCommand("Test");
        getContentPane().add(test);
        test.setBounds(125, 204, 100, 24);
        recognize.setText("Compare");
        recognize.setActionCommand("Compare");
        getContentPane().add(recognize);
        recognize.setBounds(125, 180, 100, 24);

        JScrollPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        JScrollPane1.setOpaque(true);
        getContentPane().add(JScrollPane1);
        JScrollPane1.setBounds(12, 24, 144, 132);
        JScrollPane1.getViewport().add(letters);
        letters.setBounds(0, 0, 126, 100);

        JScrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        JScrollPane2.setOpaque(true);
        getContentPane().add(JScrollPane2);
        JScrollPane2.setBounds(168, 24, 144, 132);
        JScrollPane2.getViewport().add(brokenLetters);
        brokenLetters.setBounds(0, 0, 126, 129);

        load.setText("Load");
        load.setActionCommand("Load");
        getContentPane().add(load);
        load.setBounds(12, 180, 100, 24);
        train.setText("Learn");
        train.setActionCommand("Learn");
        getContentPane().add(train);
        train.setBounds(12, 204, 100, 24);
        label = new JLabel("Broken letters");
        getContentPane().add(label);
        label.setBounds(180, 12, 144, 12);
        //}}

        //{{REGISTER_LISTENERS
        SymAction lSymAction = new SymAction();
        test.addActionListener(lSymAction);
        SymListSelection lSymListSelection = new SymListSelection();
        letters.addListSelectionListener(lSymListSelection);
        brokenLetters.addListSelectionListener(lSymListSelection);
        load.addActionListener(lSymAction);
        train.addActionListener(lSymAction);
        recognize.addActionListener(lSymAction);
        //}}
        letters.setModel(referenceListModel);
        brokenLetters.setModel(brokenListModel);
        //{{INIT_MENUS
        //}}
    }


    /**
     * The main method.
     *
     * @param args Args not really used.
     */
    public static void main(String args[]) {
        (new Main()).setVisible(true);
    }

    /**
     * The test button
     */
    private JButton test = new JButton();

    /**
     * The recognize button
     */
    private JButton recognize = new JButton();
    private JScrollPane JScrollPane1 = new JScrollPane();
    private JScrollPane JScrollPane2 = new JScrollPane();

    /**
     * The letters list box
     */
    private JList letters = new JList();
    /**
     * The broken letters list box
     */
    private JList brokenLetters = new JList();

    /**
     * The load button
     */
    private JButton load = new JButton();

    /**
     * The train button
     */
    private JButton train = new JButton();

    class SymAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Object object = event.getSource();
            if (object == test)
                test_actionPerformed();
            else if (object == load)
                load_actionPerformed();
            else if (object == train)
                train_actionPerformed();
            else if (object == recognize)
                recognize_actionPerformed(letters.getSelectedIndex(), brokenLetters.getSelectedIndex(), true);
        }
    }

    /**
     * Called to test the image.
     */
    private void test_actionPerformed() {
        String error = null;
        StringBuilder sb;
        int suchCount = 0;

        if (net == null) {
            error = "I need to be trained first!";
            return;
        }
        if(referenceListModel.size() != brokenListModel.size()){
            error = "Reference and broken listst not equels";
        }

        if(error != null){
            JOptionPane.showMessageDialog(this, error, "Error!", JOptionPane.ERROR_MESSAGE);
        }

        JDialog dialog = new JDialog(this);
        dialog.setMaximumSize(new Dimension(400, 300));
        JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        dialog.add(new JScrollPane(textArea));
        textArea.append("Test result:\n");
        for (int idx = 0; idx < referenceListModel.size(); ++idx){
            boolean result = recognize_actionPerformed(idx, idx, false);
            if(result) ++suchCount;
            CharData ds = (CharData) referenceListModel.get(idx);
            textArea.append("Character recognition '" +ds.getCharacter() +"' " + (result ? "suchesfull" : "faled") + "\n");
        }
        textArea.append("\nSuchesfull " + 100*suchCount/referenceListModel.size() + "%.");

        dialog.pack();
        dialog.setVisible(true);
    }

    class SymListSelection implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent event) {
            Object object = event.getSource();
            if (object == letters)
                letters_valueChanged(letters);
            else if(object == brokenLetters)
                letters_valueChanged(brokenLetters);
        }
    }

    /**
     * Called when a letter is selected from the list box.
     */
    private void letters_valueChanged(JList itemList) {
        if (itemList.getSelectedIndex() == -1)
            return;
        CharData selected = (CharData) itemList.getModel().getElementAt(itemList.getSelectedIndex());
        try {
            charDataPanel.setData((CharData) selected.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        charDataPanel.repaint();
    }

    /**
     * Called when the load button is pressed.
     */
    private void load_actionPerformed() {
        try {
            load_date(DATA_FILE, referenceListModel);
            letters.setSelectedIndex(0);
            load_date(BROKEN_DATA_FILE, brokenListModel);
            brokenLetters.setSelectedIndex(0);
            test_actionPerformed();
            JOptionPane.showMessageDialog(this,
                    "Loaded from '" + DATA_FILE + "'.", "Training",
                    JOptionPane.PLAIN_MESSAGE);
        }catch (Exception ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Error: " + ex, "Training",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * Called when the load button is pressed.
     */
    private void load_date(String dataFile, DefaultListModel model) throws IOException {
        InputStreamReader f;// the actual file stream
        BufferedReader r;// used to read the file line by line
        f = new InputStreamReader(this.getClass().getResourceAsStream(dataFile), StandardCharsets.UTF_8);
        r = new BufferedReader(f);
        String line;

        model.clear();

        while ((line = r.readLine()) != null) {
            CharData ds = new CharData(line.charAt(0), Main.DOWNSAMPLE_WIDTH, Main.DOWNSAMPLE_HEIGHT);
            model.addElement(ds);
            int idx = 2;
            for (int y = 0; y < ds.getHeight(); y++) {
                for (int x = 0; x < ds.getWidth(); x++) {
                    ds.setData(x, y, line.charAt(idx++) == '1');
                }
            }
        }

        r.close();
        f.close();
    }

    /**
     * Run method for the background training thread.
     */
    public void run() {
        try {
            int inputNeuron = Main.DOWNSAMPLE_HEIGHT;// * Main.DOWNSAMPLE_WIDTH;
            int outputNeuron = referenceListModel.size();

            TrainingSet set = new TrainingSet(inputNeuron);
            set.setTrainingSetCount(referenceListModel.size());

            for (int t = 0; t < referenceListModel.size(); t++) {
                int idx = 0;
                CharData ds = (CharData) referenceListModel.getElementAt(t);
                for (int y = 0; y < ds.getHeight(); y++) {
                    set.setInput(t, idx++, ds.getDoubleRow(y));
                }
            }

            net = new KohonenNetwork(inputNeuron, outputNeuron, this);
            net.setTrainingSet(set);
            net.learn();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error: " + e, "Training", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Called to update the stats, from the neural network.
     *
     * @param trial How many tries.
     * @param error The current error.
     * @param best  The best error.
     */
    public void showLernResult(long trial, double error, double best) {
        if ((((trial % 100) != 0) || (trial == 10)) && !net.halt)
            return;

        if (net.halt) {
            trainThread = null;
            train.setText("Begin Training");
            String textResult = String.format(
                    "Training has completed.%nTries :%d%nLast Error: %f%nBest Error: %f",
                    trial, error, best
            );
            JOptionPane.showMessageDialog(this, textResult, "Training", JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * Called when the train button is pressed.
     */
    private void train_actionPerformed() {
        if(referenceListModel.getSize() == 0){
            JOptionPane.showMessageDialog(this, "No letters for learning!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (trainThread == null) {
            train.setText("Stop Training");
            train.repaint();
            trainThread = new Thread(this);
            trainThread.start();
        } else {
            net.halt = true;
        }
    }

    /**
     * Called when the recognize button is pressed.
     */
    private boolean recognize_actionPerformed(int leterIndex, int brokenIndex, boolean showMessage) {
        if (net == null) {
            JOptionPane.showMessageDialog(this,
                    "I need to be trained first!", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        //entry.downSample();

        double input[] = new double[DOWNSAMPLE_HEIGHT];
        int idx = 0;
        CharData ds = (CharData) brokenListModel.getElementAt(brokenIndex);
        for (int y = 0; y < ds.getHeight(); y++) {
            input[idx++] = ds.getDoubleRow(y);
        }

        double normfac[] = new double[1];
        double synth[] = new double[1];

        int best = net.winner(input, normfac, synth);
        char map[] = mapNeurons();

        ds = (CharData) referenceListModel.get(leterIndex);
        if(showMessage) {
            String result = String.format("Network find %c(neuron %d). Result - %b",
                    map[best], best, ds.getCharacter() == map[best]);
            JOptionPane.showMessageDialog(this, result, "Result", JOptionPane.PLAIN_MESSAGE);
        }
        return ds.getCharacter() == map[best];
    }

    /**
     * Used to map neurons to actual letters.
     *
     * @return The current mapping between neurons and letters as an array.
     */
    private char[] mapNeurons() {
        char map[] = new char[referenceListModel.size()];
        double normfac[] = new double[1];
        double synth[] = new double[1];

        for (int i = 0; i < map.length; i++)
            map[i] = '?';
        for (int i = 0; i < referenceListModel.size(); i++) {
            double input[] = new double[DOWNSAMPLE_HEIGHT];
            int idx = 0;
            CharData ds = (CharData) referenceListModel.getElementAt(i);
            for (int y = 0; y < ds.getHeight(); y++) {
                input[idx++] = ds.getDoubleRow(y);
            }

            int best = net.winner(input, normfac, synth);
            map[best] = ds.getCharacter();
        }
        return map;
    }
}