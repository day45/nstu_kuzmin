/**
 * This class holds a training set for the Kohonen network.
 * This is usually the set of characters that were inputted
 * by the user.
 */

public class TrainingSet {
    // input neuron count
    private int inputCount;
    private double[][] input;
    private int trainingSetCount;

    /**
     * The constructor.
     *
     * @param inputCount  Number of input neurons
     */
    public TrainingSet(int inputCount) {
        this.inputCount = inputCount;
        trainingSetCount = 0;
    }

    /**
     * Set the number of entries in the training set. This method
     * also allocates space for them.
     *
     * @param trainingSetCount How many entries in the training set.
     */
    public void setTrainingSetCount(int trainingSetCount) {
        this.trainingSetCount = trainingSetCount;
        input = new double[trainingSetCount][inputCount];
    }

    /**
     * Get the training set data.
     *
     * @return Training set data.
     */
    public int getTrainingSetCount() {
        return trainingSetCount;
    }

    /**
     * Set one of the training set's inputs.
     *
     * @param set   The entry number
     * @param index The index(which item in that set)
     * @param value The value
     * @throws java.lang.RuntimeException
     */
    public void setInput(int set, int index, double value) throws RuntimeException {
        if ((set < 0) || (set >= trainingSetCount))
            throw (new RuntimeException("Training set out of range:" + set));
        if ((index < 0) || (index >= inputCount))
            throw (new RuntimeException("Training input index out of range:" + index));
        input[set][index] = value;
    }

    /**
     * Get an input set.
     *
     * @param set The entry requested.
     * @return The complete input set as an array.
     * @throws java.lang.RuntimeException
     */

    public double[] getInputSet(int set) throws RuntimeException {
        if ((set < 0) || (set >= trainingSetCount))
            throw (new RuntimeException("Training set out of range:" + set));
        return input[set];
    }

}