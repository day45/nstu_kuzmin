import java.util.*;

/**
 * Abstract base class for Neural Networks.
 */

abstract class Network {

    /**
     * Output neuron activations
     */
    double[] output;

    /**
     * Mean square error of the network
     */
    double totalError;

    /**
     * Number of input neurons
     */
    int inputNeuronCount;

    /**
     * Number of output neurons
     */
    int outputNeuronCount;

    /**
     * Random number generator
     */
    private Random random = new Random(System.currentTimeMillis());


    /**
     * Called to learn from training sets.
     *
     * @throws java.lang.RuntimeException
     */
    abstract public void learn()
            throws RuntimeException;

    /**
     * Calculate the length of a vector.
     * Длина вектора находится извлечением квадратного корня из суммы квадратов всех компонент вектора
     *
     * @param v vector
     * @return Vector length.
     */
    static double vectorLength(double v[]) {
        double rtn = 0.0;
        for (double aV : v) rtn += aV * aV;
        return Math.sqrt(rtn);
    }

    /**
     * Called to calculate a dot product.
     *
     * @param vec1 one vector
     * @param vec2 another vector
     * @return The dot product.
     */

    double dotProduct(double vec1[], double vec2[]) {
        int k, m, v;
        double rtn;

        rtn = 0.0;
        k = vec1.length / 4;
        m = vec1.length % 4;

        v = 0;

        while ((k--) > 0) {
            rtn += vec1[v] * vec2[v];
            rtn += vec1[v + 1] * vec2[v + 1];
            rtn += vec1[v + 2] * vec2[v + 2];
            rtn += vec1[v + 3] * vec2[v + 3];
            v += 4;
        }
        while ((m--) > 0) {
            rtn += vec1[v] * vec2[v];
            v++;
        }
        //System.out.println(rtn);
        return rtn;
    }

    /**
     * Called to randomize weights.
     *
     * @param weight A weight matrix.
     */

    void randomizeWeights(double weight[][]) {
        double r;

        int temp = (int) (3.464101615 / (2. * Math.random()));

        for (int y = 0; y < weight.length; y++) {
            for (int x = 0; x < weight[0].length; x++) {
                r = (double) random.nextInt(Integer.MAX_VALUE) + (double) random.nextInt(Integer.MAX_VALUE) -
                    (double) random.nextInt(Integer.MAX_VALUE) - (double) random.nextInt(Integer.MAX_VALUE);
                weight[y][x] = temp * r;
            }
        }
    }

}